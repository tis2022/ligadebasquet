<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\Category;
use App\Models\Team;
use App\Models\Tournament;
use App\Models\Group;
use App\Models\Player;
use App\Models\PlayerStats;
use App\Models\Assets;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'label' => 'Lista de Usuarios',
            'link' => '/usuarios'
        ]);
        
        Permission::create([
            'label' => 'Subir PDF',
            'link' => '/archivos'
        ]);

        Permission::create([
            'label' => 'Lista de Equipos',
            'link' => '/equipos'
        ]);

        Permission::create([
            'label' => 'Torneos',
            'link' => '/torneos'
        ]);

        Permission::create([
            'label' => 'Lista de Escenarios',
            'link' => '/escenarios'
        ]);

        Permission::create([
            'label' => 'Jugadores',
            'link' => '/jugadores'
        ]);

        Permission::create([
            'label' => 'Cuerpo Técnico',
            'link' => '/cuerpo-tecnico'
        ]);
        
        Permission::create([
            'label' => 'Pre-Inscripción',
            'link' => '/registro'
        ]);

        Permission::create([
            'label' => 'Lista de Roles',
            'link' => '/roles'
        ]);

        Role::create([
            'name' => 'Admin'
        ]);

        Role::create([
            'name' => 'Responsable'
        ]);

        Role::create([
            'name' => 'Delegado'
        ]);

        // Asignando permisos para rol admin
        $adminRole = Role::find(1);
        $adminRole->permissions()->attach(1);
        $adminRole->permissions()->attach(2);
        $adminRole->permissions()->attach(9);

        // Asignando permisos para rol responsable
        $responsibleRole = Role::find(2);
        $responsibleRole->permissions()->attach(1);
        $responsibleRole->permissions()->attach(3);
        $responsibleRole->permissions()->attach(4);
        $responsibleRole->permissions()->attach(5);
        $responsibleRole->permissions()->attach(2);

        // Asignando permisos para rol delegado
        $delegateRole = Role::find(3);
        $delegateRole->permissions()->attach(3);
        $delegateRole->permissions()->attach(6);
        $delegateRole->permissions()->attach(7);
        $delegateRole->permissions()->attach(8);

        Category::create([
            'name' => '+35'
        ]);

        Category::create([
            'name' => '+45'
        ]);

        Category::create([
            'name' => '+55'
        ]);

        User::create([
            'name' => 'admin',
            'lastname' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('Password1*'),
            'document_id' => '123abc',
            'role_id' => 1
        ]);

        User::create([
            'name' => 'responsable',
            'lastname' => 'resp',
            'email' => 'resp@resp.com',
            'password' => Hash::make('Password1*'),
            'document_id' => '123',
            'role_id' => 2
        ]);

        User::create([
            'name' => 'delegado',
            'lastname' => 'del',
            'email' => 'del@del.com',
            'password' => Hash::make('Password1*'),
            'document_id' => '123',
            'role_id' => 3
        ]);

        Tournament::create([
            'category_id' => 2,
            'name' => 'Torneo de prueba',
            'country' => 'Bolivia',
            'startDate' => '2022-06-17',
            'endDate' => '2022-06-30'
        ]);

        Team::create([
            'category_id' => 2,
            'delegate_id' => 3,
            'name' => 'Spurs',
            'country' => 'Bolivia',
            'enabled' => 0
        ]);

        Group::create([
            'name' => 'Grupo A',
            'registration_id' => null
        ]);

        Group::create([
            'name' => 'Grupo B',
            'registration_id' => null
        ]);

        Player::create([
            'team_id' => 1,
            'name' => 'Boris',
            'lastname' => 'Fernandez',
            'document_id' => 1234,
            'position' => 'Pivote',
            'height' => 1.86,
            'weight' => 82.2,
            'picture' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1655431413/PlayerPictures/ypirt36fusvogmmpnd9d.png'
        ]);

        Player::create([
            'team_id' => 1,
            'name' => 'Tonny',
            'lastname' => 'Fernandez',
            'document_id' => 1234,
            'position' => 'Alero',
            'height' => 1.96,
            'weight' => 82.2,
            'picture' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1655431413/PlayerPictures/ypirt36fusvogmmpnd9d.png'
        ]);

        Player::create([
            'team_id' => 1,
            'name' => 'Gabriel',
            'lastname' => 'Gamez',
            'document_id' => 1234,
            'position' => 'Guardia Central',
            'height' => 1.70,
            'weight' => 68.9,
            'picture' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1655431413/PlayerPictures/ypirt36fusvogmmpnd9d.png'
        ]);

        Player::create([
            'team_id' => 1,
            'name' => 'Marco',
            'lastname' => 'Fernandez',
            'document_id' => 1234,
            'position' => 'Pivote',
            'height' => 1.86,
            'weight' => 82.2,
            'picture' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1655431413/PlayerPictures/ypirt36fusvogmmpnd9d.png'
        ]);

        Player::create([
            'team_id' => 1,
            'name' => 'Rodrigo',
            'lastname' => 'Fernandez',
            'document_id' => 1234,
            'position' => 'Alero',
            'height' => 1.69,
            'weight' => 72.1,
            'picture' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1655431413/PlayerPictures/ypirt36fusvogmmpnd9d.png'
        ]);

        PlayerStats::create([
            'player_id' => 1,
            'playedMatches' => 10,
            'points' => 25,
            'rebounds' => 10,
            'assists' => 8,
            'steals' => 3,
            'fouls' => 2,
        ]);

        PlayerStats::create([
            'player_id' => 2,
            'playedMatches' => 10,
            'points' => 25,
            'rebounds' => 10,
            'assists' => 8,
            'steals' => 3,
            'fouls' => 2,
        ]);

        PlayerStats::create([
            'player_id' => 3,
            'playedMatches' => 10,
            'points' => 25,
            'rebounds' => 10,
            'assists' => 8,
            'steals' => 3,
            'fouls' => 2,
        ]);

        PlayerStats::create([
            'player_id' => 4,
            'playedMatches' => 10,
            'points' => 25,
            'rebounds' => 10,
            'assists' => 8,
            'steals' => 3,
            'fouls' => 2,
        ]);

        PlayerStats::create([
            'player_id' => 5,
            'playedMatches' => 10,
            'points' => 25,
            'rebounds' => 10,
            'assists' => 8,
            'steals' => 3,
            'fouls' => 2,
        ]);

        Assets::create([
            'name' => 'convocatory',
            'url' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1670357549/PlayerPictures/bxrhig7nwzdixn5asutq.pdf',
        ]);

        Assets::create([
            'name' => 'rules',
            'url' => 'https://res.cloudinary.com/djwn8itzv/image/upload/v1670372242/PlayerPictures/jqeumxlvrjeczehov8zw.pdf',
        ]);
    }
}

