<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if($user != null)
        {
            return Hash::check($request->password, $user->password)
                ? response()->json($user)
                : response()->json(["Message" => "Login Fallido"], 401);
        }
        else
        {
            response()->json(["Message" => "El email no esta registrado"], 404);
        }
    }

    public function resetPassword(Request $request)
    {
        if(User::where('email', $request->email)->exists())
        {
            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json($user);
        }
        else
        {
            return response()->json(["Message" => "El email no esta registrado"], 404);
        }
    }
}
