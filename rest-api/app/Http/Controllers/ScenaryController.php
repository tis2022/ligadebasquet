<?php

namespace App\Http\Controllers;

use App\Models\Scenary;
use Illuminate\Http\Request;

class ScenaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scenaries = Scenary::all();

        return response()->json($scenaries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $scenary = new Scenary();
        $scenary->name = $request->name; 
        $scenary->address = $request->address;
        $scenary->sportFields = $request->sportFields;
        $scenary->save();

        return response()->json([
            'message' => 'Scenary added'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Scenary  $scenary
     * @return \Illuminate\Http\Response
     */
    public function show(Scenary $scenary)
    {
        $scenary = Scenary::find($id);
        if(!empty($scenary))
        {
            return response()->json($scenary);
        }
        else
        {
            return response()->json([
                "message" => "Scenary not found"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Scenary  $scenary
     * @return \Illuminate\Http\Response
     */
    public function edit(Scenary $scenary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Scenary  $scenary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Scenary::where('id', $id)->exists())
        {
            $scenary = Scenary::find($id);
            $scenary->name = $request->name; 
            $scenary->address = $request->address;
            $scenary->sportFields = $request->sportFields;
            $scenary->save();
            
            return response()->json([
                "message" => "Scenary updated"
            ], 200);
        }
        else
        {
            return response()->json([
                "message" => "Scenary Not found"
            ], 404);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Scenary  $scenary
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Scenary::where('id', $id)->exists())
        {
            $scenary = Scenary::find($id);
            $scenary->delete();

            return response()->json([
                "message" => "Scenary deleted"
            ], 202);
        }
        else
        {
            return response()->json([
                "message" => "Scenary not found"
            ], 404);
        }
    }
}
