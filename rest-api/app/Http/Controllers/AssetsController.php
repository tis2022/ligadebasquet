<?php

namespace App\Http\Controllers;

use App\Models\Assets;
use Illuminate\Http\Request;

class AssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Assets::all();

        return response()->json($assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $asset = new Assets();
        $asset->name = $request->name;
        $asset->url = $request->url;
        $asset->save();

        return response()->json([
            'message' => 'Asset added'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function show(Assets $assets)
    {
        $asset = Assets::find($id);
        if(!empty($asset))
        {
            return response()->json($asset);
        }
        else
        {
            return response()->json([
                "message" => "Asset not found"
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function edit(Assets $assets)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Assets::where('id', $id)->exists())
        {
            $asset = Assets::find($id);
            $asset->url = $request->url;
            $asset->save();

            return response()->json([
                "message" => "Asset updated"
            ], 200);
        }
        else
        {
            return response()->json([
                "message" => "Asset Not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assets  $assets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assets $assets)
    {
        if(Assets::where('id', $id)->exists())
        {
            $asset = Assets::find($id);
            $asset->delete();

            return response()->json([
                "message" => "Asset deleted"
            ], 202);
        }
        else
        {
            return response()->json([
                "message" => "Asset not found"
            ], 404);
        }
    }
}
