## npm run dev

Runs the app in the development mode.
Open http://localhost:3000 to view it in your browser.

The page uses HMR so we can see the updates live.

## npm run lint

Runs eslint and prettier checks on the code.

## npm run test

Pending command and setup for unit tests.
