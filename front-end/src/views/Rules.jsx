
import { useEffect,useState } from "react";
import useGetAssets from "../components/uploadFiles/useGetAssets";

import PDFView from "../components/viewPDF/PDFView";


const Rules = () => {
    const [assets, setAssets] = useState();
    const getAssets = useGetAssets(setAssets);
  
    useEffect(() => {
      getAssets();
    }, [getAssets]);

  
    return (
      <PDFView
        url={assets?.[1].url}        
      />
    );
  };

export default Rules;