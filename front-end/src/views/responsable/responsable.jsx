import { Layout } from "antd";
import { Outlet } from "react-router-dom";

const { Content } = Layout;

const Responsable = () => (
  <Content className="site-layout">
    <Outlet />
  </Content>
);

export default Responsable;
