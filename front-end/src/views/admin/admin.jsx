import { Layout } from "antd";
import { Outlet } from "react-router-dom";

const { Content } = Layout;

const Admin = () => (
  <Content
    className="site-layout"
    style={{
      padding: "15px"
    }}
  >
    <Outlet />
  </Content>
);

export default Admin;
