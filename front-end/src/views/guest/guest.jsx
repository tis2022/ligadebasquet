import React from "react";
import { Outlet } from "react-router-dom";

const Guest = () => <Outlet />;

export default Guest;
