// import React from "react";
import { useEffect,useState } from "react";

import PDFView from "../../components/viewPDF/PDFView";
import useGetAssets from "../../components/uploadFiles/useGetAssets";

const Convocatoria = () => {
    const [assets, setAssets] = useState();
    const getAssets = useGetAssets(setAssets);
  
    useEffect(() => {
      getAssets();
    }, [getAssets]);

  
    return (
      <PDFView
        url={assets?.[0].url}        
      />
    );
  };
  
export default Convocatoria;
