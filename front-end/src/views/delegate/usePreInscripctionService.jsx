import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";

const enabledTeamLookup = {
  Si: true,
  No: false
};

export const teamCategoryLookup = {
  "+35": 1,
  "+45": 2,
  "+55": 3
};

const usePreInscriptionService = () => {
  const dispatch = useDispatch();
  const delegateId = useSelector(state => state.user.id);

  const handleError = useCallback(
    error => {
      const message = Array.isArray(error) ? error[1] : error.message;
      dispatch(
        updateBanner({
          message,
          type: "error"
        })
      );
    },
    [dispatch]
  );

  const saveTeamData = useCallback(
    initialValues => data => {
      fetch(`${config.baseURL}/api/teams`, {
        method: "POST",
        body: JSON.stringify({
          ...initialValues,
          ...data,
          enabled: false,
          delegate_id: delegateId
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Equipo añadido exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError, delegateId]
  );
  const updateTeamData = useCallback(
    initialValues => data => {
      fetch(`${config.baseURL}/api/teams/${initialValues.id}`, {
        method: "PUT",
        body: JSON.stringify({
          ...initialValues,
          ...data,
          enabled: enabledTeamLookup[initialValues.enabled],
          category_id: teamCategoryLookup[initialValues.category_id]
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Equipo actualizado exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError]
  );

  const enableTeam = useCallback(
    initialValues => {
      fetch(`${config.baseURL}/api/teams/${initialValues.id}`, {
        method: "PUT",
        body: JSON.stringify({
          ...initialValues,
          enabled: true
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Equipo inscrito exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError]
  );

  const savePlayers = useCallback(
    data => {
      fetch(`${config.baseURL}/api/players`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Jugador añadido exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError]
  );

  const saveStaff = useCallback(
    data => {
      fetch(`${config.baseURL}/api/staff`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Miembro del cuerpo técnico añadido exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError]
  );

  /**
   * Pre-inscripcion
   */
  const register = useCallback(
    data => {
      fetch(`${config.baseURL}/api/registration`, {
        method: "POST",
        body: JSON.stringify({ ...data, status: false }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Pre-inscripcion añadida exitosamente.",
              type: "success"
            })
          );
        })
        .catch(handleError);
    },
    [dispatch, handleError]
  );

  const getTeamRegistration = useCallback(
    (id, setRegistration) => {
      fetch(`${config.baseURL}/api/registration/${id}`, {
        method: "GET"
      })
        .then(data => data.json())
        .then(setRegistration)
        .catch(handleError);
    },
    [handleError]
  );

  return {
    saveTeamData,
    updateTeamData,
    enableTeam,
    savePlayers,
    saveStaff,
    register,
    getTeamRegistration
  };
};

export default usePreInscriptionService;
