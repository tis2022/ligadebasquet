import { Button, Input, InputNumber, Select } from "antd";
import { Field, useForm } from "react-final-form";
import PropTypes from "prop-types";
import { isEmpty } from "lodash/fp";
import Title from "antd/lib/typography/Title";

import ErrorField from "../../../components/common/ErrorField";
import useGetEnabledTeamsByDelegate from "../../../components/teams/useGetEnabledTeamsByDelegate";

const { Option } = Select;

const CoachingStaffFormBody = ({ handleSubmit, errors }) => {
  const delegateTeams = useGetEnabledTeamsByDelegate();
  const { reset, resetFieldState } = useForm();

  return (
    <div>
      {isEmpty(delegateTeams) ? (
        <Title level={3}>
          No existen equipos inscritos, complete una inscripción antes de
          registrar cuerpo tecnico
        </Title>
      ) : null}
      <Field name="name">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Nombre(s): *</span>
            <div className="form-field">
              <Input
                maxLength="25"
                placeholder="Nombres"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="lastname">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Apellido(s): *</span>
            <div className="form-field">
              <Input
                maxLength="25"
                placeholder="Apellidos"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="role">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Rol: *</div>
            <div className="form-field">
              <Select
                style={{
                  width: 150
                }}
                defaultValue={["Entrenador"]}
                placeholder="Seleccione un rol"
                status={meta.error && meta.touched ? "error" : undefined}
                {...input}
              >
                <Option value="Entrenador">Entrenador</Option>
                <Option value="Co-Entrenador">Co-Entrenador</Option>
                <Option value="Médico">Médico</Option>
              </Select>
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="document_id">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Número de documento: *</span>
            <div className="form-field">
              <InputNumber
                maxLength="10"
                min="1"
                style={{ width: 150 }}
                placeholder="Número de documento"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="team_id">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Equipo: *</span>
            <div className="form-field">
              <Select
                style={{
                  width: 150
                }}
                defaultValue={[delegateTeams?.[0]?.name]}
                placeholder="Seleccione un equipo"
                status={meta.error && meta.touched ? "error" : undefined}
                {...input}
              >
                {delegateTeams?.map(team => (
                  <Option key={team.id} value={team.id}>
                    {team.name}
                  </Option>
                ))}
              </Select>
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => {
            handleSubmit();
            reset();
            resetFieldState("name");
            resetFieldState("lastname");
            resetFieldState("role");
            resetFieldState("document_id");
            resetFieldState("team_id");
          }}
          type="primary"
          className="add-to-list-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

CoachingStaffFormBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default CoachingStaffFormBody;
