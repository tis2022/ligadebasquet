import { Modal, Card, Button } from "antd";
import { isEmpty } from "lodash/fp";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import useGetTeams, {
  teamCategoryLookup
} from "../../../components/teams/useGetTeams";
import usePreInscriptionService from "../usePreInscripctionService";

const { Meta } = Card;

const TeamEnable = () => {
  const navigate = useNavigate();
  const [registration, setRegistration] = useState();
  const { getTeamRegistration, enableTeam } = usePreInscriptionService();
  const { teamId } = useParams();
  const { getTeams } = useGetTeams();

  useEffect(() => {
    getTeamRegistration(teamId, setRegistration);
  }, [getTeamRegistration, teamId]);

  const currentTeam = registration?.[0]?.team;
  const handleOk = () => {
    navigate(-1);
  };
  const handleCancel = () => {
    navigate(-1);
  };

  return (
    <Modal
      visible
      onOk={handleOk}
      onCancel={handleCancel}
      footer={null}
      width={380}
    >
      {!isEmpty(registration?.[0]) ? (
        <>
          <Card
            style={{
              width: 320
            }}
            size="small"
            cover={<img alt="example" src={registration?.[0]?.voucher} />}
          >
            <Meta title="Nombre" description={currentTeam?.name} />
            <Meta
              title="Categoria"
              description={`${teamCategoryLookup[currentTeam.category_id]}`}
            />
            <Meta title="País" description={`${currentTeam?.country}`} />
          </Card>
          <Button
            type="primary"
            onClick={() => {
              enableTeam(registration?.[0].team);
              setTimeout(() => {
                getTeams();
              }, 1000);
              navigate(-1);
            }}
            key="approve"
          >
            Aprobar
          </Button>
        </>
      ) : (
        <p>Este equipo no realizo su preinscripcion</p>
      )}
    </Modal>
  );
};

export default TeamEnable;
