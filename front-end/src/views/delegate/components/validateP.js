import {
    applyRules,
    validateNumericField,
    validateRequired
  } from "../../../components/common/utils/validation";

  const validateName = values => {
    const nameFormat = /[a-zA-Zá-úÁ-Ú]+$/;
    
    return {
      name: nameFormat.test(values.name)
      ? undefined
      : "Solo se permite valores alfabeticos"
    };
  };
  
  const validateLastName = values => {
    const lnameFormat = /[a-zA-Zá-úÁ-Ú]+$/;
  
    return {
      lastname: lnameFormat.test(values.lastname)
      ? undefined
      : "Solo se permite valores alfabeticos"
    };
  };
  
  const validateRequiredFields = ({
    name,
    lastname,
    position,
    document_id,
    height,
    weight,
    team_id,
    picture
  }) => ({
    name: validateRequired(name),
    lastname: validateRequired(lastname),
    position: validateRequired(position),
    document_id: validateRequired(document_id),
    height: validateRequired(height),
    weight: validateRequired(weight),
    team_id: validateRequired(team_id),
    picture: validateRequired(picture)
  });
  
  const validatePlayer = applyRules([
    validateRequiredFields,
    validateName,
    validateLastName,
    validateNumericField("document_id")
  ]);
  
  export default validatePlayer;
  