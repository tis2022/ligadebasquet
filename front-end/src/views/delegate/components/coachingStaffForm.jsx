import PropTypes from "prop-types";

import BaseForm from "../../../components/common/BaseForm";
import CoachingStaffFormBody from "./CoachingStaffFormBody";
import validateCoachingStaff from "./validate";

const CoachingStaffForm = ({ saveData }) => (
  <BaseForm
    FormBody={CoachingStaffFormBody}
    title="Datos del cuerpo técnico"
    saveData={saveData}
    validate={validateCoachingStaff}
  />
);

CoachingStaffForm.propTypes = {
  saveData: PropTypes.func.isRequired
};

export default CoachingStaffForm;
