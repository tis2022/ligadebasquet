import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Form, Input, Modal, Typography } from "antd";
import PropTypes from "prop-types";

import SelectCountry from "../../../components/selectCountry/selectCountry";
import SelectCategory from "../../../components/selectCategory/selectCategory";
import { updateBanner } from "../../../store/bannerSlice";
import useGetTeams from "../../../components/teams/useGetTeams";

const { Title } = Typography;

const TeamForm = ({ saveTeamData }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { getTeams, teams } = useGetTeams();
  const { teamId } = useParams();
  const initialValues = teamId
    ? teams.filter(({ id }) => `${id}` === teamId)[0]
    : {};
  const onFinish = oldValues => data => {
    navigate(-1);
    saveTeamData(oldValues)(data);
    setTimeout(() => {
      getTeams();
    }, 1000);
  };

  const onFinishFailed = errorInfo => {
    dispatch(
      updateBanner({
        message: errorInfo,
        type: "error"
      })
    );
  };

  return (
    <Modal visible onCancel={() => navigate(-1)} footer={null}>
      <Form
        name="basic"
        initialValues={initialValues}
        onFinish={onFinish(initialValues)}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Title level={4}>Datos del Equipo</Title>
        <Form.Item
          label="Nombre del Equipo"
          name="name"
          rules={[
            {
              required: true,
              message: "Por favor ingrese el nombre del equipo"
            }
          ]}
        >
          <Input maxLength="20" />
        </Form.Item>
        <SelectCategory />
        <SelectCountry />
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Guardar Equipo
          </Button>
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16
          }}
        />
      </Form>
    </Modal>
  );
};

TeamForm.propTypes = {
  saveTeamData: PropTypes.func.isRequired
};

export default TeamForm;
