import { Button, Input, InputNumber, Select } from "antd";
import { Field, useForm } from "react-final-form";
import { isEmpty } from "lodash/fp";
import PropTypes from "prop-types";
import Title from "antd/lib/typography/Title";

import CloudinaryUpload from "../../../components/common/CloudinaryUpload";
import ErrorField from "../../../components/common/ErrorField";
import useGetEnabledTeamsByDelegate from "../../../components/teams/useGetEnabledTeamsByDelegate";

const { Option } = Select;

// const type = useSelector(state => state.user.type);
const PlayerFormBody = ({ handleSubmit, restart, errors }) => {
  const { change } = useForm();
  const delegateTeams = useGetEnabledTeamsByDelegate();

  return (
    <div>
      {isEmpty(delegateTeams) ? (
        <Title level={3}>
          No existen equipos inscritos, complete una inscripción antes de
          registrar jugadores
        </Title>
      ) : null}
      <Field name="name">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Nombre(s): *</span>
            <div className="form-field">
              <Input
                maxLength="25"
                placeholder="Nombres"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="lastname">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Apellido(s): *</span>
            <div className="form-field">
              <Input
                maxLength="25"
                placeholder="Apellidos"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="position">
        {({ input }) => (
          <div className="field-container">
            <div>Posicion: *</div>
            <div className="form-field">
              <Select
                style={{
                  width: 150
                }}
                defaultValue="Base"
                placeholder="Seleccione una posicion"
                {...input}
              >
                <Option value="Base">Base</Option>
                <Option value="Escolta">Escolta</Option>
                <Option value="Alero">Alero</Option>
                <Option value="Ala-Pivot">Ala-Pivot</Option>
                <Option value="Pivot">Pivot</Option>
              </Select>
            </div>
          </div>
        )}
      </Field>
      <Field name="document_id">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Numero de documento: *</span>
            <div className="form-field">
              <InputNumber
                maxLength="10"
                min="1"
                style={{ width: 150 }}
                placeholder="Numero de documento"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="height">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Estatura (m): *</div>
            <div className="form-field">
              <InputNumber
                min="1"
                maxLength="4"
                placeholder="Estatura del jugador"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="weight">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Peso (Kg): *</div>
            <div className="form-field">
              <InputNumber
                min="1"
                maxLength="4"
                placeholder="Peso del jugador"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="team_id">
        {({ input }) => (
          <>
            <div>Equipo: *</div>
            <Select
              style={{
                width: 150
              }}
              defaultValue={delegateTeams?.[0]?.name}
              placeholder="Seleccione un equipo"
              {...input}
            >
              {delegateTeams?.map(team => (
                <Option key={team.id} value={team.id}>
                  {team.name}
                </Option>
              ))}
            </Select>
          </>
        )}
      </Field>
      <Field name="picture">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Foto: *</span>
            <div className="form-field">
              <Input
                placeholder="Suba una foto"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              <CloudinaryUpload
                onSuccess={url => {
                  change("picture", url);
                }}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => {
            handleSubmit();
            restart();
          }}
          type="primary"
          className="add-to-list-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

PlayerFormBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  restart: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default PlayerFormBody;
