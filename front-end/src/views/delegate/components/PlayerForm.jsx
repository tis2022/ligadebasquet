import PropTypes from "prop-types";

import PlayerFormBody from "./PlayerFormBody";
import BaseForm from "../../../components/common/BaseForm";
import validatePlayer from "./validateP";

const PlayerForm = ({ saveData }) => (
  <BaseForm
    FormBody={PlayerFormBody}
    title="Datos del jugador"
    saveData={saveData}
    initialValues={{ position: "Base" }}
    validate={validatePlayer}
  />
);

PlayerForm.propTypes = {
  saveData: PropTypes.func.isRequired
};

export default PlayerForm;
