import {
  applyRules,
  validateNumericField,
  validateRequired
} from "../../../components/common/utils/validation";

const validateName = values => {
  const nameFormat = /[a-zA-Zá-úÁ-Ú]+$/;
  
  return {
    name: nameFormat.test(values.name)
    ? undefined
    : "Solo se permite valores alfabeticos"
  };
};

const validateLastName = values => {
  const lnameFormat = /[a-zA-Zá-úÁ-Ú]+$/;

  return {
    lastname: lnameFormat.test(values.lastname)
    ? undefined
    : "Solo se permite valores alfabeticos"
  };
};

const validateRequiredFields = ({
  name,
  lastname,
  document_id,
  role,
  team_id
}) => ({
  name: validateRequired(name),
  lastname: validateRequired(lastname),
  document_id: validateRequired(document_id),
  role: validateRequired(role),
  team_id: validateRequired(team_id)
});

const validateCoachingStaff = applyRules([
  validateRequiredFields,
  validateName,
  validateLastName,
  validateNumericField("document_id")
]);

export default validateCoachingStaff;
