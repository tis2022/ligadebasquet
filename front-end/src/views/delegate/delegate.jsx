import { Content } from "antd/lib/layout/layout";
import { Outlet } from "react-router-dom";

const Delegate = () => (
  <Content className="site-layout">
    <Outlet />
  </Content>
);

export default Delegate;
