import { BrowserRouter, Routes, Route } from "react-router-dom";

import App from "../App";
import UserModal from "../components/users/UserModal";
import DeleteUserModal from "../components/users/DeleteUserModal";
import Guest from "../views/guest/guest";
import UserList from "../components/users/UserList";
import TeamList from "../components/teams/TeamList";
import PlayersList from "../components/players/PlayersList";
import Tournament from "../components/tournaments/Tournament";
import PlayerStats from "../components/playerStats/PlayerStats";
import Register from "../components/register/Register";
import PlayerForm from "../views/delegate/components/PlayerForm";
import CoachingStaffForm from "../views/delegate/components/coachingStaffForm";
import usePreInscriptionService from "../views/delegate/usePreInscripctionService";
import TeamForm from "../views/delegate/components/teamForm";
import ScenaryList from "../components/scenaries/ScenaryList";
import ScenaryForm from "../components/scenaries/ScenaryForm";
import useScenaryService from "../components/scenaries/useScenaryService";
import DeleteScenaryModal from "../components/scenaries/DeleteScenaryModal";
import ResetPassword from "../components/login/ResetPassword";
import Convocatoria from "../views/convocatoria/Convocatoria";
import Rules from "../views/Rules";
import TableTeams from "../components/tableTeams/TableTeams";
import HomePage from "../components/homepage/HomePage";
import UploadForm from "../components/uploadFiles/UploadForm";
import useAssetsService from "../components/uploadFiles/useAssetsService";
import TechnicalBodyList from "../components/technicalBody/technicalBodyList";
import DeleteTeamsModal from "../components/teams/DeleteTeamsModal";
import TeamEnable from "../views/delegate/components/TeamEnable";
import RoleList from "../components/roles/RoleList";
import RoleModal from "../components/roles/RoleModal";
import DeleteRole from "../components/roles/DeleteRole";
import RolePermissions from "../components/roles/RolePermissions";
import useGetRoles from "../components/roles/useGetRoles";

const AppRoutes = () => {
  const { saveTeamData, updateTeamData, savePlayers, saveStaff, register } =
    usePreInscriptionService();
  const { saveScenary, updateScenary } = useScenaryService();
  const { updateAssets } = useAssetsService();
  useGetRoles();

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route path="cambiar-contrasena" element={<ResetPassword />} />
          <Route path="convocatoria" element={<Convocatoria />} />
          <Route path="rules" element={<Rules />} />
          <Route path="inicio" element={<HomePage />} />
          <Route path="tablaEquipos" element={<TableTeams />} />
          <Route path="usuarios" element={<UserList />}>
            <Route path="add" element={<UserModal title="Agregar Usuario" />} />
            <Route
              path=":userId/edit"
              element={<UserModal title="Editar Usuario" />}
            />
            <Route path=":userId/delete" element={<DeleteUserModal />} />
          </Route>
          <Route path="roles" element={<RoleList />}>
            <Route path="add" element={<RoleModal />} />
            <Route
              path=":roleId/edit"
              element={<RoleModal title="Editar Usuario" />}
            />
            <Route path=":roleId/delete" element={<DeleteRole />} />
            <Route path=":roleId/permisos" element={<RolePermissions />} />
          </Route>
          <Route
            path="archivos"
            element={<UploadForm saveData={updateAssets} />}
          />
          <Route path="equipos" element={<TeamList />}>
            <Route
              path="add"
              element={<TeamForm saveTeamData={saveTeamData} />}
            />
            <Route path=":teamId/delete" element={<DeleteTeamsModal />} />
            <Route
              path=":teamId/edit"
              element={<TeamForm saveTeamData={updateTeamData} />}
            />
            <Route path=":teamId/habilitar" element={<TeamEnable />} />
          </Route>
          <Route path="equipos/:teamId/jugadores" element={<PlayersList />}>
            <Route path=":playerID/stats" element={<PlayerStats />} />
          </Route>
          <Route path="equipos/:teamId/staff" element={<TechnicalBodyList />} />
          <Route
            path="jugadores"
            element={<PlayerForm saveData={savePlayers} />}
          />
          <Route
            path="cuerpo-tecnico"
            element={<CoachingStaffForm saveData={saveStaff} />}
          />
          <Route path="registro" element={<Register saveData={register} />} />
          <Route path="escenarios" element={<ScenaryList />}>
            <Route
              path="add"
              element={<ScenaryForm saveData={saveScenary} />}
            />
            <Route
              path=":scenaryId/edit"
              element={<ScenaryForm saveData={updateScenary} />}
            />
            <Route path=":scenaryId/delete" element={<DeleteScenaryModal />} />
          </Route>
          <Route path="torneos" element={<Tournament />} />
          <Route path="guest" element={<Guest />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
