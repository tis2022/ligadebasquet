import { createSlice } from "@reduxjs/toolkit";

export const bannerSlice = createSlice({
  name: "banner",
  initialState: {},
  reducers: {
    updateBanner: (_, action) => ({
      ...action.payload
    }),
    clearBanner: () => undefined
  }
});

export const { updateBanner, clearBanner } = bannerSlice.actions;
