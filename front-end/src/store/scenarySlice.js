import { createSlice } from "@reduxjs/toolkit";

export const scenarySlice = createSlice({
  name: "scenary",
  initialState: {
    scenaries: []
  },
  reducers: {
    scenaryList: (state, action) => ({
      ...state,
      scenaries: action.payload
    })
  }
});

// Action creators are generated for each case reducer function
export const { scenaryList } = scenarySlice.actions;
