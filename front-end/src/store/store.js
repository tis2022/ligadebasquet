import { configureStore } from "@reduxjs/toolkit";

import { dataSlice } from "./dataSlice";
import { userSlice } from "./userSlice";
import { bannerSlice } from "./bannerSlice";
import { scenarySlice } from "./scenarySlice";

const store = configureStore({
  reducer: {
    user: userSlice.reducer,
    data: dataSlice.reducer,
    banner: bannerSlice.reducer,
    scenary: scenarySlice.reducer
  }
});

export default store;
