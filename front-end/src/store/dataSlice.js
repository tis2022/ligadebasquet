import { createSlice } from "@reduxjs/toolkit";

export const dataSlice = createSlice({
  name: "data",
  initialState: {
    users: [],
    tournaments: [],
    playerData: {},
    groups: [],
    roles: [],
    permissions: [],
    rolePermissions: []
  },

  reducers: {
    userList: (state, action) => ({
      ...state,
      users: action.payload
    }),
    teamList: (state, action) => ({
      ...state,
      teams: action.payload
    }),
    playersList: (state, action) => ({
      ...state,
      players: action.payload
    }),
    techBodyList: (state, action) => ({
      ...state,
      staff: action.payload
    }),
    roleList: (state, action) => ({
      ...state,
      roles: action.payload
    }),
    permissionList: (state, action) => ({
      ...state,
      permissions: action.payload
    }),
    rolePermissions: (state, action) => ({
      ...state,
      rolePermissions: action.payload
    }),
    playerData: (state, action) => ({
      ...state,
      playerData: action.payload
    }),
    newTournament: (state, action) => {
      const current = [...state.tournaments];
      current.unshift(action.payload);
      return {
        ...state,
        tournaments: current
      };
    },
    removeTournament: (state, action) => {
      const current = [...state.tournaments];
      const result = current.filter(item => item.id !== action.payload);

      return {
        ...state,
        tournaments: result
      };
    },
    editTournament: (state, action) => {
      const current = [...state.tournaments];
      const result = current.map(item => {
        if (item.id === action.payload.id) {
          return {
            ...action.payload
          };
        }
        return item;
      });

      return {
        ...state,
        tournaments: result
      };
    },
    tournamentList: (state, action) => ({
      ...state,
      tournaments: action.payload
    }),
    categories: (state, action) => ({ ...state, categories: action.payload }),
    listGroups: (state, action) => ({ ...state, groups: action.payload })
  }
});

export const {
  userList,
  teamList,
  playersList,
  playerData,
  categories,
  newTournament,
  tournamentList,
  removeTournament,
  editTournament,
  listGroups,
  techBodyList,
  roleList,
  permissionList,
  rolePermissions
} = dataSlice.actions;
