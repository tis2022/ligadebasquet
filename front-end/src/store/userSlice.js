import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    type: "visitor",
    permissions: []
  },
  reducers: {
    userLogin: (state, action) => ({
      ...state,
      type: action.payload.type,
      id: action.payload.id
    }),
    userLogout: state => ({
      ...state,
      type: undefined,
      id: undefined,
      permissions: []
    }),
    getPermissions: (state, action) => ({
      ...state,
      permissions: action.payload
    })
  }
});

// Action creators are generated for each case reducer function
export const { userLogin, userLogout, getPermissions } = userSlice.actions;
