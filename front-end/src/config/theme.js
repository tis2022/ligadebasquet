import { ConfigProvider } from "antd";

export default ConfigProvider.config({
  theme: {
    primaryColor: '#020D4D',
  },
});