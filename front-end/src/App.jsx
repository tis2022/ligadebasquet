import { useState, useEffect } from "react";
import { Layout, Menu, Alert, Button } from "antd";
import { Outlet, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Login from "./components/login/login";
import Logo from "./components/logo/logo";
import { clearUserInfo, getUserInfo } from "./components/login/utils";
import useSidebarItems from "./components/hooks/useSidebarItems";
import { clearBanner } from "./store/bannerSlice";
import { userLogin, userLogout } from "./store/userSlice";
import useGetPermissions from "./components/hooks/useGetPermissions";

import "./App.css";

const { Header, Content, Footer, Sider } = Layout;

const App = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const banner = useSelector(state => state.banner);
  const hasSession = useSelector(state => state.user.id);
  const { fetchPermissions } = useGetPermissions();

  useEffect(() => {
    const sessionInfo = getUserInfo();

    if (sessionInfo && !hasSession) {
      dispatch(userLogin({ type: sessionInfo.role, id: sessionInfo.id }));
      fetchPermissions(sessionInfo.roleId);
    }
  }, [dispatch, hasSession, fetchPermissions]);

  const logout = () => {
    dispatch(userLogout());
    clearUserInfo();
    navigate("inicio");
  };

  const items = useSidebarItems();

  return (
    <Layout className="main-layout">
      <Header>
        <Menu mode="horizontal" theme="dark">
          <Menu.Item key="logo" disabled>
            <Logo />
          </Menu.Item>
          <Menu.Item key="Inicio" onClick={() => navigate("/inicio")}>
            Inicio
          </Menu.Item>
          <Menu.Item
            key="Convocatorias"
            onClick={() => navigate("/convocatoria")}
          >
            Convocatoria
          </Menu.Item>
          <Menu.Item key="rules" onClick={() => navigate("/rules")}>
            Reglas
          </Menu.Item>
          <Menu.Item key="Equipos" onClick={() => navigate("/tablaEquipos")}>
            Equipos
          </Menu.Item>
          {!hasSession && (
            <Menu.Item key="login">
              <Login />
            </Menu.Item>
          )}
          {hasSession && (
            <Menu.Item key="logout">
              <Button onClick={logout} type="primary" className="save-btn">
                Logout
              </Button>
            </Menu.Item>
          )}
        </Menu>
      </Header>
      <Content>
        <Layout className="content-layout">
          <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
            <Menu defaultSelectedKeys={["1"]} mode="inline" theme="dark">
              {items}
            </Menu>
          </Sider>
          <Content className="site-layout">
            {banner.message ? (
              <Alert
                message={banner.message}
                type={banner.type}
                closable
                onClose={() => dispatch(clearBanner())}
              />
            ) : null}
            <div className="site-layout-background">
              <Outlet />
            </div>
          </Content>
        </Layout>
      </Content>

      <Footer
        style={{
          textAlign: "center",
          paddingTop: 0
        }}
      >
        Maxibasquet ©2022
      </Footer>
    </Layout>
  );
};

export default App;
