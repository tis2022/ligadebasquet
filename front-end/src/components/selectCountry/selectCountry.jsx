import { Form, Select } from "antd";

const { Option } = Select;

const SelectCountry = () => (
  <Form.Item
    name="country"
    label="Seleccionar Pais"
    hasFeedback
    rules={[
      {
        required: true,
        message: "Porfavor selecciona tu Pais"
      }
    ]}
  >
    <Select placeholder="Porfavor selecciona un pais">
      <Option value="Argentina">Argentina</Option>
      <Option value="Bolivia">Bolivia</Option>
      <Option value="Brazil">Brazil</Option>
      <Option value="Chile">Chile</Option>
      <Option value="Colombia">Colombia</Option>
      <Option value="Ecuador">Ecuador</Option>
      <Option value="Venezuela">Venezuela</Option>
      <Option value="Paraguay">Paraguay</Option>
      <Option value="Peru">Peru</Option>
      <Option value="Uruguay">Uruguay</Option>
      <Option value="Usa">U.S.A</Option>
    </Select>
  </Form.Item>
);

export default SelectCountry;
