import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Button, Form, Input, message } from "antd";
import { find } from "lodash/fp";

import { userLogin } from "../../store/userSlice";
import login from "../../services/login";
import { setUserInfo } from "./utils";

import "./login.css";
import useGetPermissions from "../hooks/useGetPermissions";

const rolesPath = {
  1: "/usuarios",
  2: "/equipos",
  3: "/equipos"
};

const Login = () => {
  const [visible, setVisible] = useState(false);
  const { fetchPermissions } = useGetPermissions();
  const roles = useSelector(state => state.data.roles);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const showModal = () => {
    setVisible(true);
  };

  const showPasswordModal = () => {
    navigate("/cambiar-contrasena");
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const onFinish = values => {
    login(values)
      .then(data => {
        setVisible(false);
        navigate(rolesPath[String(data.role_id)] ?? "/");
        const currentRole = find(role => role.id === data.role_id, roles);
        dispatch(
          userLogin({
            type: currentRole.name,
            id: data.id
          })
        );
        setUserInfo(data.id, currentRole.name, currentRole.id);
        fetchPermissions(data.role_id);
      })
      .catch(() => {
        message.error("Error: Verifique sus credenciales");
      });
  };

  const onFinishFailed = errorInfo => {
    message.error("Failed:", errorInfo);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Iniciar Sesión
      </Button>
      <Modal
        title="Login"
        visible={visible}
        footer={null}
        onCancel={handleCancel}
        className="login-modal"
      >
        <Form
          name="basic"
          labelCol={{
            span: 8
          }}
          wrapperCol={{
            span: 14
          }}
          initialValues={{
            remember: true
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Por favor ingresa tu username o email!"
              }
            ]}
          >
            <Input maxLength="45" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Por favor ingresa tu contrasena!"
              }
            ]}
          >
            <Input.Password
              required
              minLength="8"
              maxLength="16"
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
              title="Ingrese una contraseña de minimo 8 caracteres.
              La contraseña debe tener al menos:
              Un carater mayuscula, minuscula, numero y un simbolo.
              Ejm: Contraseña@2"
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 16
            }}
          >
            <Button type="primary" htmlType="submit">
              Ingresar
            </Button>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16
            }}
          >
            <Button type="primary" key="contrasena" onClick={showPasswordModal}>
              Cambiar contraseña
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default Login;
