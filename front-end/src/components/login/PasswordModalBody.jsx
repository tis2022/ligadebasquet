import { Field } from "react-final-form";
import { Button, Input } from "antd";
import isEmpty from "lodash/fp/isEmpty";
import PropTypes from "prop-types";
import ErrorField from "../common/ErrorField";

const PasswordModalBody = ({ handleSubmit, errors }) => (
  <div className="reset-password-modal">
    <Field name="email">
      {({ input, meta }) => (
        <div className="field-container">
          <div>Correo electronico: *</div>
          <div className="form-field">
            <Input
              minLength="15"
              maxLength="45"
              status={meta.error && meta.touched ? "error" : undefined}
              placeholder="Correo electronico"
              {...input}
            />
            {meta.error && meta.touched && <ErrorField error={meta.error} />}
          </div>
        </div>
      )}
    </Field>
    <Field name="password">
      {({ input, meta }) => (
        <div className="field-container">
          <div>Contraseña: *</div>
          <div className="form-field">
            <Input
              required
              minLength="8"
              maxLength="16"
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
              title="Ingrese una contraseña de minimo 8 caracteres.
              La contraseña debe tener al menos:
              Un carater mayuscula, minuscula, numero y un simbolo.
              Ejm: Contraseña@2"
              status={meta.error && meta.touched ? "error" : undefined}
              type="password"
              placeholder="Contraseña"
              {...input}
            />
            {meta.error && meta.touched && <ErrorField error={meta.error} />}
          </div>
        </div>
      )}
    </Field>
    <Field name="confirmPassword">
      {({ input, meta }) => (
        <div className="field-container">
          <div>Confirmar contraseña: *</div>
          <div className="form-field">
            <Input
              required
              minLength="8"
              maxLength="16"
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
              title="Ingrese una contraseña de minimo 8 caracteres.
              La contraseña debe tener al menos:
              Un carater mayuscula, minuscula, numero y un simbolo.
              Ejm: Contraseña@2"
              status={meta.error && meta.touched ? "error" : undefined}
              type="password"
              placeholder="Confirmar contraseña"
              {...input}
            />
            {meta.error && meta.touched && <ErrorField error={meta.error} />}
          </div>
        </div>
      )}
    </Field>
    <div className="modal-footer">
      <Button
        onClick={handleSubmit}
        type="primary"
        className="save-btn"
        disabled={!isEmpty(errors)}
      >
        Guardar
      </Button>
    </div>
  </div>
);

PasswordModalBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};
export default PasswordModalBody;
