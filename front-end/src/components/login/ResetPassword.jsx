import {message, Modal} from "antd";
import { Form } from "react-final-form";
import React, {useCallback, } from "react";
import {useNavigate} from "react-router-dom";
import {validatePassword} from "../users/validate";
import PasswordModalBody from "./PasswordModalBody";
import {resetPassword} from "../../services/login";

const ResetPassword = () => {
    const navigate = useNavigate();

    const onCancel = useCallback(() => navigate("/"), [navigate])
    const onPasswordFinish = useCallback(
        formData => {

            const transformedData = {
                ...formData
            };

            resetPassword(transformedData)
                .then(() => {
                    message.info("Contraseña cambiada exitosamente")
                    onCancel()
                })
                .catch(() => {
                    message.error("No se pudo cambiar la contraseña")
                })
        },
        [onCancel]
    )

    return (
        <Modal title="Cambiar contraseña" visible footer={null} onCancel={onCancel}>
        <Form
            onSubmit={onPasswordFinish}
            validate={validatePassword}
            render={({ handleSubmit, errors }) => (
                <PasswordModalBody handleSubmit={handleSubmit} errors={errors} />
            )}
        />
        </Modal>
    )
}

export default ResetPassword;

