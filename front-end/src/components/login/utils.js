import isEmpty from "lodash/fp/isEmpty";

export const setUserInfo = (id, role, roleId) => {
  sessionStorage.setItem("id", id);
  sessionStorage.setItem("role", role);
  sessionStorage.setItem("roleId", roleId);
};

export const getUserInfo = () => {
  const sessionInfo = {
    id: sessionStorage.getItem("id"),
    role: sessionStorage.getItem("role"),
    roleId: sessionStorage.getItem("roleId")
  };

  return !isEmpty(sessionInfo) ? sessionInfo : undefined;
};

export const clearUserInfo = () => {
  sessionStorage.removeItem("id");
  sessionStorage.removeItem("role");
  sessionStorage.removeItem("roleId");
};
