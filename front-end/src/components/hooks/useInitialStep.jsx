import useTeamInfo from "./useTeamInfo";

const useInitialStep = () => {
  const { teamId, players } = useTeamInfo();
  let step = 0;
  if (teamId) {
    if (players?.length > 0) {
      step = 2;
    }
    step = 1;
  }

  return step;
};

export default useInitialStep;
