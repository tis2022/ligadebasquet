import { useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";

import { config } from "../../services/api-service";
import { permissionList, rolePermissions } from "../../store/dataSlice";
import { getPermissions } from "../../store/userSlice";

const useGetPermissions = () => {
  const dispatch = useDispatch();

  const fetchPermissions = useCallback(
    roleId => {
      fetch(`${config.baseURL}/api/permissions/${roleId}`)
        .then(data => data.json())
        .then(permissions =>
          permissions.map(({ label, link, id }) => ({
            label,
            link,
            id
          }))
        )
        .then(permissions => dispatch(getPermissions(permissions)));
    },
    [dispatch]
  );

  const fetchPermissionsByRole = useCallback(
    rolId => {
      fetch(`${config.baseURL}/api/permissions/${rolId}`)
        .then(data => data.json())
        .then(permissions =>
          permissions.map(({ label, link, id }) => ({
            label,
            link,
            id
          }))
        )
        .then(permissions => dispatch(rolePermissions(permissions)));
    },
    [dispatch]
  );

  const fetchAllPermissions = useCallback(() => {
    fetch(`${config.baseURL}/api/permissions`)
      .then(data => data.json())
      .then(permissions =>
        permissions.map(({ label, link, id }) => ({
          label,
          link,
          id
        }))
      )
      .then(permissions => dispatch(permissionList(permissions)));
  }, [dispatch]);

  useEffect(() => {
    fetchAllPermissions();
  }, [fetchPermissions, fetchAllPermissions]);

  return {
    fetchPermissions,
    fetchPermissionsByRole,
    fetchAllPermissions
  };
};

export default useGetPermissions;
