import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { config } from "../../services/api-service";
import { tournamentList } from "../../store/dataSlice";

const useGetTournaments = () => {
  const dispatch = useDispatch();

  return useCallback(() => {
    fetch(`${config.baseURL}/api/tournaments`)
      .then(data => data.json())
      .then(data => {
        dispatch(tournamentList(data));
      });
  }, [dispatch]);
};

export default useGetTournaments;
