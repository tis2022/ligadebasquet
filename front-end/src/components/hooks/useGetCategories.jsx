import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { config } from "../../services/api-service";

import { categories } from "../../store/dataSlice";

const useGetCategories = () => {
  const dispatch = useDispatch();

  return useCallback(() => {
    fetch(`${config.baseURL}/api/categories`)
      .then(data => data.json())
      .then(data => {
        dispatch(categories(data));
      });
  }, [dispatch]);
};

export default useGetCategories;
