import { useSelector } from "react-redux";

import useGetTeams from "../teams/useGetTeams";

const useTeamInfo = () => {
  const delegateId = useSelector(state => state.user.id);
  const teams = useGetTeams() ?? [];

  const filteredTeams = teams.filter(team => team.delegate_id === delegateId);
  // Use to review if team has players.
  const teamId = filteredTeams[0]?.id ?? 2;
  const players = filteredTeams[0]?.players;

  return { teamId, players };
};

export default useTeamInfo;
