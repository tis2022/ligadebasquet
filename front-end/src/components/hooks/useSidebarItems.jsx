import { useMemo } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { Menu } from "antd";

import useGetPermissions from "./useGetPermissions";

const idByRoleType = {
  admin: 1,
  responsable: 2,
  delegado: 3
};

const useSidebarItems = () => {
  const userType = useSelector(state => state.user.type);
  useGetPermissions(idByRoleType[userType]);
  const permissions = useSelector(state => state.user.permissions);

  return useMemo(
    () =>
      permissions?.map(item => (
        <Menu.Item key={item.link}>
          <Link to={item.link} className="logo">
            <span>{item.label}</span>
          </Link>
        </Menu.Item>
      )),
    [permissions]
  );
};

export default useSidebarItems;
