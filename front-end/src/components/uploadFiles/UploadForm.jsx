import PropTypes from "prop-types";
import { useEffect, useState } from "react";

import BaseForm from "../common/BaseForm";
import UploadConvocatory from "./UploadConvocatory";
import UploadRules from "./UploadRules";
import useGetAssets from "./useGetAssets";

const UploadForm = ({ saveData }) => {
  const [assets, setAssets] = useState();
  const getAssets = useGetAssets(setAssets);

  useEffect(() => {
    getAssets();
  }, [getAssets]);
  const initialValues =
    assets?.reduce(
      (accum, current) => ({
        ...accum,
        [current.name]: current.url
      }),
      {}
    ) ?? {};

  return (
    <>
      <BaseForm
        FormBody={UploadConvocatory}
        title="Subir Convocatoria Maxi-Basquet"
        saveData={saveData(1)}
        initialValues={{url:initialValues.convocatory}}        
      />
      <BaseForm
        FormBody={UploadRules}
        title="Subir Reglamento Maxi-Basquet"
        saveData={saveData(2)}
        initialValues={{url:initialValues.rules}}
      />
    </>
  );
};

UploadForm.propTypes = {
  saveData: PropTypes.func.isRequired
};

export default UploadForm;
