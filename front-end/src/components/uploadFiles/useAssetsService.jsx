import { useCallback } from "react";
import { useDispatch } from "react-redux";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetAssets from "./useGetAssets";

const useAssetsService = () => {
  const dispatch = useDispatch();
  const getAssets = useGetAssets();

  const handleError = useCallback(
    error => {
      dispatch(
        updateBanner({
          message: error.message ?? "Algo salio mal",
          type: "error"
        })
      );
    },
    [dispatch]
  );

  const updateAssets = useCallback(
    id => asset => {
      fetch(`${config.baseURL}/api/assets/${id}`, {
        method: "PUT",
        body: JSON.stringify(asset),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Archivo añadido exitosamente.",
              type: "success"
            })
          );
          getAssets();
        })
        .catch(handleError);
    },
    [dispatch, handleError, getAssets]
  );

  return { updateAssets };
};

export default useAssetsService;
