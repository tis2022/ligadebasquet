import { Field, useForm } from "react-final-form";
import { Button, Input } from "antd";
import { isEmpty } from "lodash/fp";
import PropTypes from "prop-types";

import ErrorField from "../common/ErrorField";
import CloudinaryUpload from "../common/CloudinaryUpload";

const UploadRules = ({ handleSubmit, errors }) => {
  const { change } = useForm();

  return (
    <div>

      <Field name="url">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Reglamento: *</span>
            <div className="form-field">
              <Input
                placeholder="Suba el reglamento en PDF"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              <CloudinaryUpload
                onSuccess={url => {
                  change("url", url);
                }}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>

      <div className="modal-footer">
        <Button
          onClick={() => {
            handleSubmit();
          }}
          type="primary"
          className="add-to-list-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

UploadRules.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default UploadRules;