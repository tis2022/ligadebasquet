import { useCallback } from "react";
import { config } from "../../services/api-service";

const useGetAssets = setAssets => useCallback(
    () =>
      fetch(`${config.baseURL}/api/assets`)
        .then(data => data.json())
        .then(setAssets),
    [setAssets]
  );

export default useGetAssets;
