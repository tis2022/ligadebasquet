import { Form, Select } from "antd";
import { useEffect } from "react";
import { useSelector } from "react-redux";

import useGetCategories from "../hooks/useGetCategories";

const { Option } = Select;

const SelectCategory = () => {
  const getCategories = useGetCategories();

  useEffect(() => {
    getCategories();
  }, [getCategories]);

  const categories = useSelector(state => state.data.categories ?? []);
  const options = categories.map(category => (
    <Option key={category.id} value={category.id}>
      {category.name}
    </Option>
  ));

  return (
    <Form.Item
      name="category_id"
      label="Seleccionar Categoria"
      hasFeedback
      rules={[
        {
          required: true,
          message: "Porfavor selecciona tu Categoria"
        }
      ]}
    >
      <Select placeholder="Porfavor selecciona una Categoria">{options}</Select>
    </Form.Item>
  );
};

export default SelectCategory;
