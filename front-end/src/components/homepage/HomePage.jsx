import React from "react";
import "./homepage.css";

const HomePage = () => (
  <div className="container">
    <div className="rulescontainer">
      <p>
        El Maxibásquetbol fue creado en Buenos Aires, Argentina, en el año 1969.
        Ese año, un grupo de ex jugadores participó de un jamboree de
        exhibición. Unos meses más tarde, a instancias del Sr. Eduardo Rodríguez
        Lamas, se creaba la UNIÓN ARGENTINA DE VETERAÑOS DE BÁSQUETBOL DE LA
        REPÚBLICA ARGENTINA.
        <br />
        En los años siguientes se fueron desarrollando las primeras reglas de la
        nueva categoría. El primer torneo internacional se llevó a cabo en
        Argentina en 1978 con la realización del Campeonato Sudamericano,
        auspiciado porla Confederación Sudamericana de Básquetbol (Consubasquet)
        dependiente de FIBA. Durante los años 70 las autoridades argentinas
        comenzaron con la promoción en otros paises, logrando que en poco tiempo
        fuera una práctica usual en Uruguay, Brasil, Chile, Peru y algunos
        países de Centro América, tales como Costa Rica y Guatemala. Con el
        tiempo, esos países crearon sus propias entidades federativas.
        <br />
        En 1980 se realizó el primer Campeonato Panamericano de Maxibásquetbol
        en Buenos Aires, bajo el auspicio de la Confederación Panamericana de
        Básquetbol (Copaba) dependiente de FIBA. En 1984, un grupo de
        entusiastas deportistas propuso organizar un Evento Internacional
        Deportivo para atletas de edad madura. Las autoridades argentinas
        ayudaron en la iniciativa y asi se realizaron por primera vez en
        Toronto, Canadá, los Masters Games. Fue la primera prueba del interés
        mundial por la categoría.
        <br />A mediados de los años 80, la Federación Argentina buscaba una
        denominación para la categoría que definiera sin dudas ni confusiones a
        sus jugadores, ya que hasta ese momento se los denominaba Masters,
        Senior, Veteraños, etc. El Secretario de la entidad, el Sr. Hilario
        Briones, propuso la denominación de maxibásquetbol como oposición al
        minibásquetbol, que es la primera categoría. Fue inmediatamente aceptada
        y en pocos años recorrió el mundo.
        <br />
        El prefijo latino Maxi, (que significa lo mayor) y la palabra Básquetbol
        componen la denominación Maxibásquetbol. En 1985 nace entonces la
        Federación Argentina de Maxibásquetbol. En 1987 surge la idea de
        organizar un Campeonato Mundial para la categoría. Argentina se ofrece
        como sede, ya que había sido pionera en ese tipo de eventos
        internacionales. Durante los siguientes cuatro años los representantes
        argentinos viajaron recorriendo el mundo para promocionar el evento. En
        1991 se realizó en Buenos Aires el 1° CAMPEONATO MUNDIAL DE
        MAXIBÁSQUETBOL, con 32 equipos de 8 países: Uruguay, USA, Guatemala,
        Brasil, Rusia, Estonia, Finlandia y Argentina.
        <br />
        El 21 de Agosto de 1991 se firmaron los estatutos fundacionales de la
        INTERNATIONAL MAXIBASKETBALL FEDERATION, entre los 8 países presentes.
        Su primer Presidente fue el Sr. Eduardo Rodríguez Lamas, mejor conocido
        como el creador del Maxibásquetbol. Al siguiente año, la ciudad de Las
        Vegas -USA- fue designada como sede para el 2° CAMPEONATO MUNDIAL DE
        MAXIBÁSQUETBOL que se desarrolló en 1993. En esa oportunidad el número
        de equipos creció a 34 y esta vez sólo en la rama masculina, y a 10
        países: Argentina, Estonia, Letonia, Uruguay, Costa Rica, Guatemala,
        Honduras, Peru, Chile y USA. Cuarenta equipos masculinos de 10 países
        estuvieron presentes en el 3° CAMPEONATO MUNDIAL DE MAXIBÁSQUETBOL
        realizado en San José, Costa Rica, en 1995: Argentina, Uruguay, Perú,
        Letonia, Guatemala, Brasil, Chile, Lituania, USA y Costa Rica.
      </p>
    </div>
  </div>
);

export default HomePage;
