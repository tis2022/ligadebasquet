import PropTypes from "prop-types";

const PDFView = ({url}) => (
  <object
    aria-label="PDF"
    data={url}
    type="application/pdf"
    width="800"
    height="750"
  />
);

PDFView.propTypes = {
    url: PropTypes.string.isRequired
}

export default PDFView;
