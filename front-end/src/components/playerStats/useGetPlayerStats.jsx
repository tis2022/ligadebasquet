import { useCallback } from "react";

import { config } from "../../services/api-service";

const useGetPlayerStats = (playerID, setData) =>
  useCallback(() => {
    fetch(`${config.baseURL}/api/stats?player_id=${playerID}`)
      .then(data => data.json())
      .then(data => setData(data[0]));
  }, [playerID, setData]);

export default useGetPlayerStats;
