import { Modal, Card } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import useGetPlayerStats from "./useGetPlayerStats";

const { Meta } = Card;

const PlayerStats = () => {
  const [data, setData] = useState();
  const navigate = useNavigate();
  const { playerID } = useParams();
  const playerData = useSelector(state => state.data.playerData);

  const getPlayerStats = useGetPlayerStats(playerID, setData);
  useEffect(() => {
    getPlayerStats();
  }, [getPlayerStats]);

  const handleOk = () => {
    navigate(-1);
  };

  const handleCancel = () => {
    navigate(-1);
  };

  return (
    <Modal
      visible
      onOk={handleOk}
      onCancel={handleCancel}
      footer={null}
      width={350}
    >
      <Card
        style={{
          width: 300
        }}
        size="small"
        cover={<img alt="example" src={playerData?.picture} />}
      >
        <Meta title="Partidos jugados" description={data?.playedMatches} />
        <Meta title="Puntos" description={`${data?.points}`} />
        <Meta title="Rebotes" description={`${data?.rebounds}`} />
        <Meta title="Asistencias" description={`${data?.assists}`} />
        <Meta title="Robos" description={`${data?.steals}`} />
        <Meta title="Faltas" description={`${data?.fouls}`} />
      </Card>
    </Modal>
  );
};

export default PlayerStats;
