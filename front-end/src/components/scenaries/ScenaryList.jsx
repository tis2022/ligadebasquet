import { useMemo, useCallback, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import { Button, Table } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import useGetScenaries from "./useGetScenaries";

const defaultColumns = [
  {
    title: "Nombre Escenario",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Dirección",
    dataIndex: "address",
    key: "address"
  },
  {
    title: "# De Canchas",
    dataIndex: "sportFields",
    key: "sportFields"
  }
];

const ScenaryList = () => {
  const scenaries = useSelector(state => state.scenary.scenaries);
  const getScenaries = useGetScenaries();

  useEffect(() => {
    getScenaries();
  }, [getScenaries]);

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: (_, record) => (
        <>
          <Button
            type="primary"
            success
            onClick={() => handleClick("edit", record)}
            key="edit"
            className="edit-btn"
          >
            Editar
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => handleClick("delete", record)}
            key="delete"
          >
            Eliminar
          </Button>
        </>
      )
    }),
    [handleClick]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      <Button
        type="primary"
        onClick={() => navigate("add")}
        icon={<PlusOutlined />}
      >
        Agregar Escenario
      </Button>
      <Table dataSource={scenaries} columns={columns} />
      <Outlet />
    </div>
  );
};

export default ScenaryList;
