import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { config } from "../../services/api-service";

import { scenaryList } from "../../store/scenarySlice";

const useGetScenaries = () => {
  const dispatch = useDispatch();

  return useCallback(() => {
    fetch(`${config.baseURL}/api/scenaries`)
      .then(data => data.json())
      .then(data => {
        dispatch(scenaryList(data));
      });
  }, [dispatch]);
};

export default useGetScenaries;
