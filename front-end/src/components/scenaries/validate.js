import { applyRules, validateRequired } from "../common/utils/validation";

const validateName = values => {
  const nameFormat = /[a-zA-Zá-úÁ-Ú1-9]+$/;

  return {
    name: nameFormat.test(values.name)
      ? undefined
      : "Solo se permite valores alfabeticos"
  };
};

const validateRequiredFields = ({ name, address }) => ({
  name: validateRequired(name),
  address: validateRequired(address)
});

const validatePositiveFieldNumber = values =>
  values.sportFields <= 0
    ? {
        sportFields: "El número de canchas debe ser mayor a 0"
      }
    : undefined;

const validate = applyRules([
  validateRequiredFields,
  validateName,
  validatePositiveFieldNumber
]);

export default validate;
