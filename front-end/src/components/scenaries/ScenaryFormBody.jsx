import { useEffect } from "react";
import { Field } from "react-final-form";
import { useNavigate } from "react-router-dom";
import { Button, Input, InputNumber } from "antd";
import { isEmpty } from "lodash/fp";
import PropTypes from "prop-types";

import ErrorField from "../common/ErrorField";
import useGetScenaries from "./useGetScenaries";

const ScenaryFormBody = ({ handleSubmit, errors }) => {
  const getScenaries = useGetScenaries();
  const navigate = useNavigate();

  useEffect(() => {
    getScenaries();
  }, [getScenaries]);

  return (
    <div>
      <Field name="name">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Nombre: *</span>
            <div className="form-field">
              <Input
                maxLength="35"
                placeholder="Nombre del escenario deportivo"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
                width={300}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="address">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Dirección: *</div>
            <div className="form-field">
              <Input
                maxLength="40"
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Dirección"
                width={500}
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="sportFields">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Número De Canchas: *</div>
            <div className="form-field">
              <InputNumber
                maxLength="15"
                style={{ width: 200 }}
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Número de Canchas"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => {
            navigate(-1);
          }}
          className="add-to-list-btn"
        >
          Cancelar
        </Button>
        <Button
          onClick={() => {
            handleSubmit();
            navigate(-1);
          }}
          type="primary"
          className="add-to-list-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

ScenaryFormBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default ScenaryFormBody;
