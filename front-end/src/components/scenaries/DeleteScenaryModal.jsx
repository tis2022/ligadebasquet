import { Modal } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetScenaries from "./useGetScenaries";

const DeleteScenaryModal = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { scenaryId } = useParams();
  const getScenaries = useGetScenaries();

  const onSubmit = useCallback(() => {
    fetch(`${config.baseURL}/api/scenaries/${scenaryId}`, {
      method: "DELETE"
    }).then(() => {
      navigate(-1);
      getScenaries();
      dispatch(
        updateBanner({
          message: "Escenario eliminado exitosamente.",
          type: "success"
        })
      );
    });
  }, [dispatch, getScenaries, navigate, scenaryId]);

  return (
    <Modal
      title="Eliminar Escenario"
      visible
      onOk={onSubmit}
      onCancel={() => navigate(-1)}
    >
      Esta seguro que desea eliminar el escenario?
    </Modal>
  );
};

export default DeleteScenaryModal;
