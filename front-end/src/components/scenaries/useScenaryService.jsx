import { useCallback } from "react";
import { useDispatch } from "react-redux";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetScenaries from "./useGetScenaries";

const useScenaryService = () => {
  const dispatch = useDispatch();
  const getScenaries = useGetScenaries();

  const handleError = useCallback(
    error => {
      dispatch(
        updateBanner({
          message: error.message ?? "Algo malio sal",
          type: "error"
        })
      );
    },
    [dispatch]
  );

  const saveScenary = useCallback(
    data => {
      fetch(`${config.baseURL}/api/scenaries`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Escenario añadido exitosamente.",
              type: "success"
            })
          );
          getScenaries();
        })
        .catch(handleError);
    },
    [dispatch, handleError, getScenaries]
  );

  const updateScenary = useCallback(
    data => {
      fetch(`${config.baseURL}/api/scenaries/${data.id}`, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          dispatch(
            updateBanner({
              message: "Escenario añadido exitosamente.",
              type: "success"
            })
          );
          getScenaries();
        })
        .catch(handleError);
    },
    [dispatch, handleError, getScenaries]
  );

  return { saveScenary, updateScenary };
};

export default useScenaryService;
