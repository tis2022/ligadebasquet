import { Modal } from "antd";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import BaseForm from "../common/BaseForm";
import ScenaryFormBody from "./ScenaryFormBody";
import validate from "./validate";

const ScenaryForm = ({ saveData }) => {
  const navigate = useNavigate();
  const { scenaryId } = useParams();
  const scenaries = useSelector(state => state.scenary.scenaries);
  const initialValues = scenaryId
    ? scenaries.filter(({ id }) => `${id}` === scenaryId)
    : {};

  return (
    <Modal visible footer={null} onCancel={() => navigate(-1)}>
      <BaseForm
        FormBody={ScenaryFormBody}
        title="Datos del escenario"
        saveData={saveData}
        initialValues={initialValues[0]}
        validate={validate}
      />
    </Modal>
  );
};

ScenaryForm.propTypes = {
  saveData: PropTypes.func.isRequired
};

export default ScenaryForm;
