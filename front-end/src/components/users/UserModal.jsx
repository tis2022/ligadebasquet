import { useCallback } from "react";
import { Form } from "react-final-form";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "antd";
import PropTypes from "prop-types";
import isEmpty from "lodash/fp/isEmpty";

import UserModalBody from "./UserModalBody";
import useGetUsers from "./useGetUsers";
import { updateBanner } from "../../store/bannerSlice";
import { config } from "../../services/api-service";
import validate from "./validate";

const defaultValues = {
  role_id: 2
};

const UserModal = ({ title }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { userId } = useParams();
  const userDataFromStore = useSelector(
    state =>
      state.data.users.filter(user => user.id === parseInt(userId, 10))[0]
  );
  const initialValues = userDataFromStore ?? defaultValues;
  const isEdit = !isEmpty(userDataFromStore);
  const getUsers = useGetUsers();

  const onSubmit = useCallback(
    formData => {
      const newUserFields = !userId
        ? {
            role_id: formData.role_id
          }
        : {};

      const transformedData = {
        ...formData,
        ...newUserFields
      };

      const endpointTail = userId ? `/${userId}` : "";
      const message = userId
        ? "Usuario editado exitosamente."
        : "Usuario añadido exitosamente.";

      fetch(`${config.baseURL}/api/users${endpointTail}`, {
        method: userId ? "PUT" : "POST",
        body: JSON.stringify(transformedData),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          navigate(-1);
          dispatch(
            updateBanner({
              message,
              type: "success"
            })
          );
          getUsers();
        })
        .catch(error => {
          dispatch(
            updateBanner({
              message: error.message,
              type: "error"
            })
          );
        });
    },
    [dispatch, navigate, getUsers, userId]
  );

  return (
    <Modal title={title} visible footer={null} onCancel={() => navigate(-1)}>
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        validate={validate(isEdit)}
        render={({ handleSubmit, errors }) => (
          <UserModalBody
            handleSubmit={handleSubmit}
            errors={errors}
            isEdit={isEdit}
          />
        )}
      />
    </Modal>
  );
};

UserModal.propTypes = {
  title: PropTypes.string.isRequired
};

export default UserModal;
