import { Modal } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetUsers from "./useGetUsers";

const DeleteUserModal = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { userId } = useParams();
  const getUsers = useGetUsers();

  const onSubmit = useCallback(() => {
    fetch(`${config.baseURL}/api/users/${userId}`, {
      method: "DELETE"
    }).then(() => {
      navigate(-1);
      getUsers();
      dispatch(
        updateBanner({
          message: "Usuario eliminado exitosamente.",
          type: "success"
        })
      );
    });
  }, [dispatch, getUsers, navigate, userId]);

  return (
    <Modal
      title="Eliminar Usuario"
      visible
      onOk={onSubmit}
      onCancel={() => navigate(-1)}
    >
      Esta seguro que desea eliminar al usuario?
    </Modal>
  );
};

export default DeleteUserModal;
