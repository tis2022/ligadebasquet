import { useCallback, useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";
import { Table, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { groupBy } from "lodash/fp";

import useGetUsers from "./useGetUsers";
import useGetRoles from "../roles/useGetRoles";

import "antd/dist/antd.css";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Apellido",
    dataIndex: "lastname",
    key: "lastname"
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email"
  },
  {
    title: "Rol",
    dataIndex: "role_id",
    key: "role_id"
  }
];

function UserList() {
  const users = useSelector(state => state.data.users);
  const { roles } = useGetRoles();
  const rolesLookup = groupBy("id", roles);
  const mappedUserData = users.map(user => ({
    ...user,
    role_id: rolesLookup[user.role_id][0].name
  }));
  const getUsers = useGetUsers();

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: (_, record) => (
        <>
          <Button
            type="primary"
            success
            onClick={() => handleClick("edit", record)}
            key="edit"
            className="edit-btn"
          >
            Editar
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => handleClick("delete", record)}
            key="delete"
          >
            Eliminar
          </Button>
        </>
      )
    }),
    [handleClick]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      <Button
        type="primary"
        onClick={() => navigate("add")}
        icon={<PlusOutlined />}
      >
        Agregar Usuario
      </Button>
      <Table dataSource={mappedUserData} columns={columns} />
      <Outlet />
    </div>
  );
}

export default UserList;
