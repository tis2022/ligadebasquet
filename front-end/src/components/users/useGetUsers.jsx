import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { config } from "../../services/api-service";

import { userList } from "../../store/dataSlice";

const useGetUsers = () => {
  const dispatch = useDispatch();
  const currentRole = useSelector(state => state.user.id);

  return useCallback(() => {
    fetch(`${config.baseURL}/api/users`)
      .then(data => data.json())
      .then(users =>
        users.map(user => ({
          ...user,
          document_id: parseInt(user.document_id, 10)
        }))
      )
      .then(data => {
        const filteredData = data.filter(
          user => user.role_id !== 1 && user.role_id !== currentRole
        );
        dispatch(userList(filteredData));
      });
  }, [currentRole, dispatch]);
};

export default useGetUsers;
