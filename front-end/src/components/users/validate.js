import {
  applyRules,
  validateRequired,
  validateNumericField
} from "../common/utils/validation";

const validateEmail = values => {
  const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

  return {
    email: mailformat.test(values.email)
      ? undefined
      : "Correo electrónico ínvalido"
  };
};

const passwordValidation = values =>
  values.password === values.confirmPassword
    ? undefined
    : "Las contraseñas no coinciden";

const checkPassword = values => ({
  email: validateRequired(values.email),
  password: passwordValidation(values),
  confirmPassword: passwordValidation(values)
});

const validateName = values => {
  const nameFormat = /[a-zA-Zá-úÁ-Ú]+$/;

  return {
    name: nameFormat.test(values.name)
      ? undefined
      : "Solo se permite valores alfabeticos"
  };
};

const validateLastName = values => {
  const lnameFormat = /[a-zA-Zá-úÁ-Ú]+$/;

  return {
    lastname: lnameFormat.test(values.lastname)
      ? undefined
      : "Solo se permite valores alfabeticos"
  };
};

const validateRequiredFields = ({
  name,
  lastname,
  email,
  document_id,
  role_id
}) => ({
  name: validateRequired(name),
  lastname: validateRequired(lastname),
  email: validateRequired(email),
  document_id: validateRequired(document_id),
  role_id: validateRequired(role_id)
});

const validateConfirmPassword = ({ password, confirmPassword }) =>
  password !== confirmPassword
    ? {
        password: "Los campos de contraseña no coinciden",
        confirmPassword: "Los campos de contraseña no coinciden"
      }
    : {};

export const validatePassword = applyRules([checkPassword, validateEmail]);

const commonRules = [
  validateRequiredFields,
  validateNumericField("document_id"),
  validateName,
  validateLastName,
  validateEmail
];

const validateAdd = applyRules([...commonRules, validateConfirmPassword]);

const validateEdit = applyRules(commonRules);

const validate = isEdit => (isEdit ? validateEdit : validateAdd);

export default validate;
