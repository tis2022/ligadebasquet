import { Field } from "react-final-form";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

import { Button, Input, InputNumber, Select } from "antd";
import isEmpty from "lodash/fp/isEmpty";

import ErrorField from "../common/ErrorField";

const UserModalBody = ({ handleSubmit, errors, isEdit }) => {
  const navigate = useNavigate();
  const roles = useSelector(state => state.data.roles.filter(role => role.id !== 1));  
  const roleOptions = roles.map(role => ({
    label: role.name,
    value: role.id
  }));
  // const type = useSelector(state => state.user.type);

  return (
    <div>
      <Field name="name">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Nombre(s): *</div>
            <div className="form-field">
              <Input
                required
                minLength="3"
                maxLength="25"
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Nombres"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="lastname">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Apellido(s): *</div>
            <div className="form-field">
              <Input
                required
                minLength="3"
                maxLength="25"
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Apellidos"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="email">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Correo electronico: *</div>
            <div className="form-field">
              <Input
                required
                minLength="15"
                maxLength="45"
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Correo Electronico"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="document_id">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Numero de documento: *</div>
            <div className="form-field">
              <InputNumber
                required
                minLength="7"
                maxLength="10"
                min="1"
                style={{ width: 120 }}
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Numero de documento"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="role_id">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Rol: *</div>
            <div className="form-field">
              <Select
                value={input.value}
                style={{ width: 120 }}
                onChange={input.onChange}
                status={meta.error && meta.touched ? "error" : undefined}
                options={roleOptions}
              />
              {meta.error && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      {!isEdit && (
        <Field name="password">
          {({ input, meta }) => (
            <div className="field-container">
              <div>Contraseña: *</div>
              <div className="form-field">
                <Input
                  type="password"
                  minLength="8"
                  maxLength="16"
                  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                  title="Ingrese una contraseña de minimo 8 caracteres.
              La contraseña debe tener al menos:
              Un carater mayuscula, minuscula, numero y un simbolo.
              Ejm: Contraseña@2"
                  status={meta.error && meta.touched ? "error" : undefined}
                  placeholder="Contraseña"
                  {...input}
                />
                {meta.error && meta.touched && (
                  <ErrorField error={meta.error} />
                )}
              </div>
            </div>
          )}
        </Field>
      )}
      {!isEdit && (
        <Field name="confirmPassword">
          {({ input, meta }) => (
            <div className="field-container">
              <div>Confirmar Contraseña: *</div>
              <div className="form-field">
                <Input
                  type="password"
                  required
                  minLength="8"
                  maxLength="16"
                  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                  title="Ingrese una contraseña de minimo 8 caracteres.
              La contraseña debe tener al menos:
              Un carater mayuscula, minuscula, numero y un simbolo.
              Ejm: Contraseña@2"
                  status={meta.error && meta.touched ? "error" : undefined}
                  placeholder="Contraseña"
                  {...input}
                />
                {meta.error && meta.touched && (
                  <ErrorField error={meta.error} />
                )}
              </div>
            </div>
          )}
        </Field>
      )}
      <div className="modal-footer">
        <Button
          onClick={() => navigate("/usuarios")}
          type="text"
          className="cancel-btn"
        >
          Cancelar
        </Button>
        <Button
          onClick={handleSubmit}
          type="primary"
          className="save-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

UserModalBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  isEdit: PropTypes.bool.isRequired
};

export default UserModalBody;
