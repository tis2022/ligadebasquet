import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { config } from "../../services/api-service";

const useGetTeamsByDelegate = () => {
  const delegateId = useSelector(state => state.user.id);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(`${config.baseURL}/api/teams?delegate=${delegateId}`)
      .then(res => res.json())
      .then(setData);
  }, [delegateId]);

  return data;
};

export default useGetTeamsByDelegate;
