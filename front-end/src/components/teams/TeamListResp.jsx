import { useCallback, useMemo, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { Table, Button, Space } from "antd";

import useGetTeams from "./useGetTeams";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Categoria",
    dataIndex: "category_id",
    key: "category_id"
  },
  {
    title: "País",
    dataIndex: "country",
    key: "country"
  },
  {
    title: "Habilitado",
    dataIndex: "enabled",
    key: "enabled"
  }
];

const TeamListResp = () => {
  const { teams, getTeams } = useGetTeams();

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );
  useEffect(() => {
    getTeams();
  }, [getTeams]);

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: (_, record) =>
        record.enabled !== "Si" ? (
          <Space size="middle">
            <Button
              type="primary"
              success
              onClick={() => handleClick("habilitar", record)}
              key="enable"
              className="edit-btn"
            >
              Habilitar
            </Button>
          </Space>
        ) : (
          <p>Inscrito</p>
        )
    }),
    [handleClick]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      <Table dataSource={teams} columns={columns} />
      <Outlet />
    </div>
  );
};

export default TeamListResp;
