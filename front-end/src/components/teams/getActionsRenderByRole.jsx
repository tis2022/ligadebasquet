import { Button, Space } from "antd";

const getActionsRenderByRole = handleClick => ({
  Delegado: (_, record) => {
    const { enabled } = record;

    return (
      <Space size="middle">
        {enabled !== "Si" ? (
          <>
            <Button
              type="primary"
              success
              onClick={() => handleClick("edit", record)}
              key="edit"
              className="edit-btn"
            >
              Editar
            </Button>
            <Button
              type="primary"
              danger
              onClick={() => handleClick("delete", record)}
              key="delete"
            >
              Eliminar
            </Button>
          </>
        ) : null}
        <Button
          type="primary"
          onClick={() => handleClick("jugadores", record)}
          key="players"
        >
          Jugadores
        </Button>
        <Button
          type="primary"
          className="btnTeams"
          onClick={() => handleClick("staff", record)}
          key="staff"
        >
          Cuerpo Técnico
        </Button>
      </Space>
    );
  },
  Responsable: (_, record) => {
    const { enabled } = record;

    return enabled !== "Si" ? (
      <Space size="middle">
        <Button
          type="primary"
          success
          onClick={() => handleClick("habilitar", record)}
          key="enable"
          className="edit-btn"
        >
          Habilitar
        </Button>
      </Space>
    ) : (
      <p>Inscrito</p>
    );
  }
});

export default getActionsRenderByRole;
