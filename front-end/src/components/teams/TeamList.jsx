import { useCallback, useMemo, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Table, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import useGetTeams from "./useGetTeams";
import getActionsRenderByRole from "./getActionsRenderByRole";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Categoria",
    dataIndex: "category_id",
    key: "category_id"
  },
  {
    title: "País",
    dataIndex: "country",
    key: "country"
  },
  {
    title: "Habilitado",
    dataIndex: "enabled",
    key: "enabled"
  }
];

const TeamList = () => {
  const { teams, getTeams } = useGetTeams();
  const { type: role, id } = useSelector(state => state.user);
  const filteredTeams =
    role === "Responsable"
      ? teams
      : teams?.filter(team => team.delegate_id === id);

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );

  useEffect(() => {
    getTeams();
  }, [getTeams]);

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: getActionsRenderByRole(handleClick)[role]
    }),
    [handleClick, role]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      {role !== "Responsable" && (
        <Button
          type="primary"
          success
          onClick={() => navigate("add")}
          key="addTeam"
          className="add-btn"
          icon={<PlusOutlined />}
        >
          Agregar Equipo
        </Button>
      )}
      <Table dataSource={filteredTeams} columns={columns} />
      <Outlet />
    </div>
  );
};

export default TeamList;
