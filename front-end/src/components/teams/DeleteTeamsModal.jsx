import { Modal } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetTeams from "./useGetTeams";

const DeleteTeamsModal = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { teamId } = useParams();
  const { getTeams } = useGetTeams();

  const onSubmit = useCallback(() => {
    fetch(`${config.baseURL}/api/teams/${teamId}`, {
      method: "DELETE"
    }).then(() => {
      navigate(-1);
      getTeams();
      dispatch(
        updateBanner({
          message: "Equipo eliminado exitosamente.",
          type: "success"
        })
      );
    });
  }, [dispatch, getTeams, navigate, teamId]);

  return (
    <Modal
      title="Eliminar Equipo"
      visible
      onOk={onSubmit}
      onCancel={() => navigate(-1)}
    >
      ¿Está seguro que desea eliminar este equipo?
    </Modal>
  );
};

export default DeleteTeamsModal;
