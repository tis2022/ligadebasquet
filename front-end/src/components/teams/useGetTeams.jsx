import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { config } from "../../services/api-service";
import { teamList } from "../../store/dataSlice";

export const teamCategoryLookup = {
  1: "+35",
  2: "+45",
  3: "+55"
};
export const mapTeamsCategory = data =>
  data.map(team => ({
    ...team,
    category_id: teamCategoryLookup[team.category_id],
    enabled: team.enabled ? "Si" : "No"
  }));

const useGetTeams = () => {
  const dispatch = useDispatch();

  const getTeams = useCallback(() => {
    fetch(`${config.baseURL}/api/teams`)
      .then(data => data.json())
      .then(mapTeamsCategory)
      .then(data => {
        dispatch(teamList(data));
      });
  }, [dispatch]);

  useEffect(() => {
    getTeams();
  }, [getTeams]);

  const teams = useSelector(state => state.data.teams);

  return {
    teams,
    getTeams
  };
};

export default useGetTeams;
