import useGetTeamsByDelegate from "./useGetTeamsByDelegate";

const useGetEnabledTeamsByDelegate = () => {
  const teamsByDelegate = useGetTeamsByDelegate();

  const enabledTeams = teamsByDelegate.filter(team => team.enabled !== 0);

  return enabledTeams;
};

export default useGetEnabledTeamsByDelegate;
