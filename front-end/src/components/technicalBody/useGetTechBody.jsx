import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { config } from "../../services/api-service";
import { techBodyList } from "../../store/dataSlice";

const useGetTechBody = teamId => {
  const dispatch = useDispatch();

  useEffect(() => {
    fetch(`${config.baseURL}/api/staff?team=${teamId}`)
      .then(data => data.json())
      .then(data => {
        dispatch(techBodyList(data));
      });
  }, [dispatch, teamId]);

  const technicalBody = useSelector(state => state.data.staff);
  return technicalBody;
};

export default useGetTechBody;
