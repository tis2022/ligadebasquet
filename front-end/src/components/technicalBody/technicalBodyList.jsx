import { useCallback } from "react";
import { Outlet, useParams, useNavigate } from "react-router-dom";
import { Table, Button } from "antd";
import useGetTechBody from "./useGetTechBody";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Apellido",
    dataIndex: "lastname",
    key: "lastname"
  },
  {
    title: "Especialidad",
    dataIndex: "role",
    key: "role"
  }
];
const TechnicalBodyList = () => {
  const { teamId } = useParams();
  const staff = useGetTechBody(teamId);

  const navigate = useNavigate();
  const handleBack = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  return (
    <div className="App">
      <Button
        type="primary"
        success
        onClick={handleBack}
        key="back"
        className="edit-btn"
      >
        Volver
      </Button>
      <Table dataSource={staff} columns={defaultColumns} />
      <Outlet />
    </div>
  );
};

export default TechnicalBodyList;
