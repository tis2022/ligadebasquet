import { applyRules, validateRequired } from "../common/utils/validation";

const validateRequiredFields = ({ tournament_id, team_id }) => ({
  tournament_id: validateRequired(tournament_id),
  team_id: validateRequired(team_id)
});

const validateRegistration = applyRules([validateRequiredFields]);

export default validateRegistration;
