import { useEffect } from "react";
import { Field, useForm } from "react-final-form";
import { useSelector } from "react-redux";
import { Button, Input, InputNumber, Select } from "antd";
import { isEmpty } from "lodash/fp";
import PropTypes from "prop-types";

import useGetTournament from "../hooks/useGetTournament";
import useGetTeamsByDelegate from "../teams/useGetTeamsByDelegate";
import ErrorField from "../common/ErrorField";
import CloudinaryUpload from "../common/CloudinaryUpload";

const { Option } = Select;

const RegisterFormBody = ({ handleSubmit, errors }) => {
  const { change, reset, resetFieldState } = useForm();
  const getTournaments = useGetTournament();

  useEffect(() => {
    getTournaments();
  }, [getTournaments]);

  const tournaments = useSelector(state => state.data.tournaments);
  const delegateTeams = useGetTeamsByDelegate();

  return (
    <div>
      <Field name="tournament_id">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Campeonato: *</span>
            <div className="form-field">
              <Select
                style={{
                  width: 150
                }}
                defaultValue={[tournaments?.[0]?.name]}
                placeholder="Seleccione un equipo"
                status={meta.error && meta.touched ? "error" : undefined}
                {...input}
              >
                {tournaments?.map(tournament => (
                  <Option key={tournament.id} value={tournament.id}>
                    {tournament.name}
                  </Option>
                ))}
              </Select>
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="team_id">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Equipo: *</span>
            <div className="form-field">
              <Select
                style={{
                  width: 150
                }}
                defaultValue={[delegateTeams?.[0]?.name]}
                placeholder="Seleccione un equipo"
                status={meta.error && meta.touched ? "error" : undefined}
                {...input}
              >
                {delegateTeams
                  ?.filter(team => team.enabled === 0)
                  .map(team => (
                    <Option key={team.id} value={team.id}>
                      {team.name}
                    </Option>
                  ))}
              </Select>
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="voucher">
        {({ input, meta }) => (
          <div className="field-container">
            <span>Comprobante: *</span>
            <div className="form-field">
              <Input
                placeholder="Suba una foto del comprobante de pago"
                {...input}
                status={meta.error && meta.touched ? "error" : undefined}
              />
              <CloudinaryUpload
                onSuccess={url => {
                  change("voucher", url);
                }}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <Field name="transaction">
        {({ input, meta }) => (
          <div className="field-container">
            <div>
              Ingrese el número del comprobante de su deposito a Maxi-Basquet: *
            </div>
            <div className="form-field">
              <InputNumber
                min="1"
                maxLength="15"
                style={{ width: 200 }}
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Número de transacción"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => {
            handleSubmit();
            reset();
            resetFieldState("tournament_id");
            resetFieldState("team_id");
          }}
          type="primary"
          className="add-to-list-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

RegisterFormBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default RegisterFormBody;
