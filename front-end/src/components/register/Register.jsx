import PropTypes from "prop-types";

import BaseForm from "../common/BaseForm";
import RegisterFormBody from "./RegisterFormBody";
import validateRegistration from "./validate";

const Register = ({ saveData }) => (
  <BaseForm
    FormBody={RegisterFormBody}
    title="Pre-Inscripción a torneos"
    saveData={saveData}
    validate={validateRegistration}
  />
);

Register.propTypes = {
  saveData: PropTypes.func.isRequired
};

export default Register;
