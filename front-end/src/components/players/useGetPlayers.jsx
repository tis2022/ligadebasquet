import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { config } from "../../services/api-service";
import { playersList } from "../../store/dataSlice";

const useGetPlayers = teamId => {
  const dispatch = useDispatch();

  useEffect(() => {
    fetch(`${config.baseURL}/api/players?team=${teamId}`)
      .then(data => data.json())
      .then(data => {
        dispatch(playersList(data));
      });
  }, [dispatch, teamId]);

  const players = useSelector(state => state.data.players);

  return players;
};

export default useGetPlayers;
