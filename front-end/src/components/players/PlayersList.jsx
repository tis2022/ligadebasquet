import { useCallback } from "react";
import { Outlet, useParams, useNavigate, Link } from "react-router-dom";
import { Table, Button } from "antd";
import { useDispatch } from "react-redux";

import useGetPlayers from "./useGetPlayers";
import { playerData } from "../../store/dataSlice";

const defaultColumns = [
  {
    title: "Apellido",
    dataIndex: "lastname",
    key: "lastname"
  },
  {
    title: "Altura",
    dataIndex: "height",
    key: "height"
  },
  {
    title: "Posición",
    dataIndex: "position",
    key: "position"
  }
];

const PlayersList = () => {
  const dispatch = useDispatch();
  const columns = [
    {
      title: "Nombre",
      dataIndex: "name",
      key: "name",
      render: (value, record) => (
        <Link
          to={`${record.id}/stats`}
          onClick={() => {
            dispatch(playerData(record));
          }}
        >
          <span>{value}</span>
        </Link>
      )
    },
    ...defaultColumns
  ];

  const { teamId } = useParams();
  const players = useGetPlayers(teamId);

  const navigate = useNavigate();
  const handleBack = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  return (
    <div className="App">
      <Button
        type="primary"
        success
        onClick={handleBack}
        key="back"
        className="edit-btn"
      >
        Volver
      </Button>
      <Table dataSource={players} columns={columns} />
      <Outlet />
    </div>
  );
};

export default PlayersList;
