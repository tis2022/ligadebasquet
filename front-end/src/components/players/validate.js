import {
  applyRules,
  validateNumericField,
  validateRequired
} from "../common/utils/validation";

const validateRequiredFields = ({
  name,
  lastname,
  document_id,
  height,
  weight,
  picture
}) => ({
  name: validateRequired(name),
  lastname: validateRequired(lastname),
  document_id: validateRequired(document_id),
  height: validateRequired(height),
  weight: validateRequired(weight),
  picture: validateRequired(picture)
});

const validate = applyRules([
  validateRequiredFields,
  validateNumericField("document_id")
]);

export default validate;
