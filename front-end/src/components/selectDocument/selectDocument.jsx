import { Form, Select } from "antd";

const { Option } = Select;

const SelectDocument = props => {
  const { ...extraProps } = props;
  return (
    <Form.Item
      name="document"
      label="Seleccionar documento"
      hasFeedback
      rules={[
        {
          required: true,
          message: "Porfavor selecciona tu documento"
        }
      ]}
      {...extraProps}
    >
      <Select placeholder="Porfavor selecciona un tipo de documento">
        <Option value="ci">CI</Option>
        <Option value="dni">DNI</Option>
        <Option value="passport">Pasaporte</Option>
      </Select>
    </Form.Item>
  );
};

export default SelectDocument;
