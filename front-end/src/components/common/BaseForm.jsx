import { Typography } from "antd";
import PropTypes from "prop-types";
import { Form } from "react-final-form";

const { Title } = Typography;

const BaseForm = ({
  className = "",
  FormBody,
  customBodyProps,
  title,
  saveData,
  initialValues,
  validate
}) => (
  <div className={`form-list ${className}`}>
    <Title level={4}>{title}</Title>
    <Form
      onSubmit={saveData}
      initialValues={initialValues}
      validate={validate}
      render={({ handleSubmit, form, errors }) => (
        <FormBody
          handleSubmit={handleSubmit}
          restart={form?.restart}
          errors={errors}
          {...customBodyProps}
        />
      )}
    />
  </div>
);

BaseForm.propTypes = {
  className: PropTypes.string,
  FormBody: PropTypes.func.isRequired,
  customBodyProps: PropTypes.object,
  title: PropTypes.string,
  saveData: PropTypes.func.isRequired,
  initialData: PropTypes.array,
  initialValues: PropTypes.object,
  validate: PropTypes.func
};

export default BaseForm;
