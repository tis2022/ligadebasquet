import PropTypes from "prop-types";
import { Tooltip } from "antd";
import { WarningTwoTone } from "@ant-design/icons";

const ErrorField = ({ error }) => (
  <Tooltip title={error}>
    <WarningTwoTone className="form-icon" twoToneColor="#f02416" />
  </Tooltip>
);

ErrorField.propTypes = {
  error: PropTypes.string
};

export default ErrorField;
