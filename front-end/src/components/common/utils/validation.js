import defaults from "lodash/defaults";

export const applyRules = validationRules => values =>
  validationRules.reduce(
    (previousValue, currentRule) =>
      defaults(previousValue, currentRule(values)),
    {}
  );

export const validateRequired = value =>
  typeof value === "undefined" ? "Campo requerido" : undefined;

export const validateNumericField = key => values => ({
  [key]: Number.isInteger(values[key])
    ? undefined
    : "Solo se permite valores númericos"
});
