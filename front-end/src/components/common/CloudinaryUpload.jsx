import { Button } from "antd";
import PropTypes from "prop-types";

const CloudinaryUpload = ({ onSuccess }) => {
  const uploadWidget = () => {
    // eslint-disable-next-line
    cloudinary.openUploadWidget(
      { cloudName: "djwn8itzv", uploadPreset: "maxibasquet" },
      (error, result) => {
        if (result.event === "success") {
          onSuccess(result.info.secure_url);
        }
      }
    );
  };

  return (
    <Button type="primary" id="upload" onClick={uploadWidget}>
      Subir
    </Button>
  );
};

CloudinaryUpload.propTypes = {
  onSuccess: PropTypes.func
};

export default CloudinaryUpload;
