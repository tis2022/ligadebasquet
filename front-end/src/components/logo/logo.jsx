import { Link } from "react-router-dom";

import "./logo.css";

const Logo = () => (
  <Link to="/" className="logo">
    <img src="/images/logo.png" className="logo-image" alt="Maxibasquet" />
    <span className="text-logo">Maxibasquet</span>
  </Link>
);

export default Logo;
