import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { listGroups } from "../../store/dataSlice";
import { getGroups } from "../../services/group";

const useGetGroups = () => {
  const dispatch = useDispatch();

  return useCallback(() => {
    getGroups()
    .then(data => {
      dispatch(listGroups(data));
    });
  }, [dispatch]);
};

export default useGetGroups;
