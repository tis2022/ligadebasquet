import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { newTournament } from "../../store/dataSlice";
import { addTournament } from "../../services/tournament";

const useAddTournament = () => {
  const dispatch = useDispatch();

  return useCallback((values) => {
    addTournament(values)
    .then(data => {
      dispatch(newTournament(data));
    });
  }, [dispatch]);
};

export default useAddTournament;
