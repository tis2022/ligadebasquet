import { useEffect, useState } from "react";
import { Button, Form, Input, Modal, DatePicker } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import PropTypes from "prop-types";
import SelectCategory from "../selectCategory/selectCategory";
import useAddTournament from "./useAddTournament";
import SelectCountry from "../selectCountry/selectCountry";

const { RangePicker } = DatePicker;

export const TournamentForm = ({
  visible,
  onCreate,
  onCancel,
  initialValues,
  title = "Crear Campeonato",
  okText = "Crear"
}) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (initialValues) {
      form.setFieldsValue(initialValues);
    }
  }, [form, initialValues]);

  return (
    <Modal
      visible={visible}
      forceRender
      title={title}
      okText={okText}
      cancelText="Cancelar"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onCreate(values);
          })
          .catch(info => {
            // eslint-disable-next-line no-console
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: "public"
        }}
      >
        <Form.Item
          name="name"
          label="Nombre del campeonato"
          rules={[
            {
              required: true,
              message: "Por favor ingresar un nombre"
            }
          ]}
        >
          <Input maxLength="25" />
        </Form.Item>
        <Form.Item
          name="dates"
          label="Fecha del campeonato"
          rules={[
            {
              required: true,
              message: "Por favor ingresar las fechas"
            }
          ]}
        >
          <RangePicker
            placeholder={["Fecha de Inicio", "Fecha de Finalizacion"]}
          />
        </Form.Item>
        <SelectCategory />
        <SelectCountry />
      </Form>
    </Modal>
  );
};

TournamentForm.propTypes = {
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  onCreate: PropTypes.func,
  initialValues: PropTypes.object,
  okText: PropTypes.string,
  title: PropTypes.string
};

const TournamentFormModal = () => {
  const [visible, setVisible] = useState(false);

  const addTournament = useAddTournament();

  const onCreate = values => {
    const data = {
      name: values.name,
      category_id: values.category_id,
      startDate: values.dates[0].format("YYYY-MM-DD"),
      endDate: values.dates[1].format("YYYY-MM-DD"),
      country: values.country
    };
    addTournament(data);
    setVisible(false);
  };

  return (
    <div>
      <Button
        type="primary"
        onClick={() => {
          setVisible(true);
        }}
        icon={<PlusOutlined />}
      >
        Crear Campeonato
      </Button>
      <TournamentForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};

export default TournamentFormModal;
