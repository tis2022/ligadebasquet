import {
  ScheduleOutlined,
  ExclamationCircleOutlined,
  TeamOutlined,
  SettingOutlined,
  EditOutlined,
  DeleteOutlined,
  GlobalOutlined
} from "@ant-design/icons";
import { Card, Modal } from "antd";
import PropTypes from "prop-types";
import { useState } from "react";
import { useSelector } from "react-redux";
import moment from "moment";

import "./TournamentCard.css";
import { TournamentForm } from "./TournamentForm";
import useDeleteTournament from "./useDeleteTournament";
import useUpdateTournament from "./useUpdateTournament";
import TournamentDetail from "./TournamentDetail";

const { Meta } = Card;

const confirm = onOk => {
  Modal.confirm({
    title: "Eliminar Campeonato",
    icon: <ExclamationCircleOutlined />,
    content: "Esta seguro de eliminar el campeonato?",
    okText: "Eliminar",
    cancelText: "Cancelar",
    onOk
  });
};

const imagesCard = {
  2: "/images/ball_blue.webp",
  4: "/images/ball_pink.jpeg"
};

const TournamentCard = ({
  id,
  name,
  startDate,
  endDate,
  category_id,
  country
}) => {
  const [visible, setVisible] = useState(false);
  const [visibleDetail, setVisibleDetail] = useState(false);
  const deleteTournament = useDeleteTournament();
  const updateTournament = useUpdateTournament();
  const categories = useSelector(state => state.data.categories) || [];
  const category = categories.filter(item => item.id === category_id);

  const handleDelete = () => {
    deleteTournament(id);
  };

  const handleUpdate = values => {
    const data = {
      id,
      name: values.name,
      category_id: values.category_id,
      startDate: values.dates[0].format("YYYY-MM-DD"),
      endDate: values.dates[1].format("YYYY-MM-DD"),
      country: values.country
    };

    updateTournament(data);
    setVisible(false);
  };

  return (
    <>
      <Card
        className="tournament-card"
        cover={
          <img
            style={{ width: "100%", height: "252px" }}
            alt="tournament"
            src={imagesCard[category_id] || "/images/ball_orange.jpeg"}
          />
        }
        actions={[
          <SettingOutlined key="setting" onClick={() => setVisibleDetail(true)}/>,
          <EditOutlined key="edit" onClick={() => setVisible(true)} />,
          <DeleteOutlined key="delete" onClick={() => confirm(handleDelete)} />
        ]}
      >
        <Meta title={name} />
        <div className="detail">
          <ScheduleOutlined />
          {`Inicio -  ${startDate}`}
        </div>
        <div className="detail">
          <ScheduleOutlined />
          {`Finalizacion - ${endDate}`}
        </div>
        <div className="detail">
          <TeamOutlined />
          {category[0]?.name}
        </div>
        <div className="detail">
          <GlobalOutlined />
          {country}
        </div>
      </Card>

      <TournamentForm
        visible={visible}
        onCancel={() => setVisible(false)}
        onCreate={handleUpdate}
        title="Editar Campeonato"
        okText="Editar"
        initialValues={{
          name,
          category_id,
          country,
          dates: [moment(startDate), moment(endDate)]
        }}
      />
      <TournamentDetail visible={visibleDetail}  onClose={() => setVisibleDetail(false)} id={id} />
    </>
  );
};

TournamentCard.propTypes = {
  name: PropTypes.string,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  category_id: PropTypes.number,
  id: PropTypes.number,
  country: PropTypes.string
};

export default TournamentCard;
