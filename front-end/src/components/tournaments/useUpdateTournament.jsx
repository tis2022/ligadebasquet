import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { editTournament } from "../../store/dataSlice";
import { updateTournament } from "../../services/tournament";

const useUpdateTournament = () => {
  const dispatch = useDispatch();

  return useCallback((values) => {
    updateTournament(values)
    .then(data => {
      dispatch(editTournament(data));
    });
  }, [dispatch]);
};

export default useUpdateTournament;
