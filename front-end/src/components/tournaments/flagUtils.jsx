import ReactCountryFlag from "react-country-flag";


const countryMap = {

    Argentina: 'AR',

    Bolivia: 'BO',

    Brazil: 'BR',

    Chile: 'CL',

    Colombia: 'CO',

    Ecuador: 'EC',

    Venezuela: 'VE',

    Paraguay: 'PY',

    Peru: 'PE',

    Uruguay: 'UY',

    Usa: 'US',

}

export const mapFlag = (country) => countryMap[country]


export const renderFlagColumn = (text) => (

    <ReactCountryFlag countryCode={mapFlag(text)} svg />

  )