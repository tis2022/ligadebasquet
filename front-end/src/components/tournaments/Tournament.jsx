import { Typography, List } from "antd";
import { useSelector } from "react-redux";
import { useEffect } from "react";

import TournamentCard from "./TournamentCard";
import TournamentFormModal from "./TournamentForm";
import useGetTournaments from "../hooks/useGetTournament";
import "./Tournament.css";

const { Title } = Typography;

const Tournament = () => {
  const getTournaments = useGetTournaments();
  const tournaments = useSelector(state => state.data.tournaments);

  useEffect(() => {
    getTournaments();
  }, [getTournaments]);

  return (
    <div>
      <div className="title-content">
        <Title level={2}>Campeonatos</Title>
        <TournamentFormModal />
      </div>
      <List
        grid={{
          gutter: 16,
          column: 4
        }}
        dataSource={tournaments}
        renderItem={item => (
          <List.Item key={item.id}>
            <TournamentCard {...item} />
          </List.Item>
        )}
      />
    </div>
  );
};

export default Tournament;
