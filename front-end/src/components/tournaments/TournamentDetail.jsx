import { Modal, List, Card, Typography, Button, Breadcrumb } from "antd";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getRegistrationsbyTournament } from "../../services/registration";
import GroupTable from "./GroupTable";

import "./TournamentDetail.css";
import useGetGroups from "./useGetGroups";

const { Title } = Typography;

const TeamList = ({ teams }) => (
  <List
    grid={{
      gutter: 16,
      column: 7,
    }}
    dataSource={teams}
    renderItem={(item) => (
      <List.Item>
        <Card className="card-team">
          {item.name}
        </Card>
      </List.Item>
    )}
  />
);

TeamList.propTypes = {
  teams: PropTypes.array,
}

const TournamentDetail = ({
  visible,
  onClose,
  id,
}) => {
  const getGrouos = useGetGroups();

  const [registrations, setRegistrations] = useState([]);
  const [matchA, setMatchA] = useState([]);
  const [matchB, setMatchB] = useState([]);
  const groups = useSelector(state => state.data.groups);
  
  
  useEffect(() => {
    getGrouos();
  }, [getGrouos]);

  useEffect(() => {
    getRegistrationsbyTournament(id).then(response => {
      setRegistrations(response)
    // eslint-disable-next-line no-console
    }).catch((e) => console.log(e))
  }, [id]);

  const teams = registrations.map(item => item.team);
  const groupA = teams.splice(0,(teams.length/2));
  const groupB = teams.splice(0, teams.length);

  const formatDataMatches = (data) => {
    const newData = data.map(item => {
       const [item1, item2] = item;
       if(item1 && item2) {
         return {
           ...item1,
           id2: item2.id,
           name2: item2.name,
           category_id2: item2.category_id,
           country2: item2.country,
           dateHour: "Por definirse...",
           tags: "Pendiente..."
         }
       }
         return {
           ...item[0],
           dateHour: "Por definirse...",
           tags: "Pendiente..."
         }
       
    })
    return newData;
  }

  const handleMatchGroupA = () => {
    const matchesA = [];
    let index = 0;

    let secondIndex = 2;
    while(index !== groupA.length) {
      const res =  groupA.slice(index, secondIndex);
      matchesA.push(res);
      if(res.length === 1) {
        break;
      }
      index = secondIndex;
      secondIndex+=1;
      if(groupA.length === 2){
        break;
      } 
    }
   
    setMatchA(formatDataMatches(matchesA));
  }

  const handleMatchGroupB = () => {
    const matchesB = [];
    let index = 0;

    let secondIndex = 2;
    while(index !== groupB.length) {
      const res =  groupB.slice(index, secondIndex);
      matchesB.push(res);
      if(res.length === 1) {
        break;
      }
      index = secondIndex;
      secondIndex+=1;
      if(groupB.length === 2){
        break;
      } 
    }
    setMatchB(formatDataMatches(matchesB));
  }

  const handleMatch = () => {
    handleMatchGroupA();
    handleMatchGroupB();
  };

  return (
    <Modal
      visible={visible}
      title="Datos del Campeonato"
      okText="Guardar"
      cancelText="Cancelar"
      onOk={onClose}
      onCancel={onClose}
      width="80%"
    >
     <div className="groups">
      <Title level={5}>Equipos Regsitrados</Title>
       <div>
         {groups[0]?.name}
         <TeamList teams={groupA} />
       </div>
       <div>
         {groups[1]?.name}
         <TeamList teams={groupB} />
       </div>
     </div>
      <Breadcrumb />
      <Button disabled={teams.length} onClick={handleMatch}>Generar Match de Equipos</Button>
      <Breadcrumb />
      {
        matchA && <GroupTable data={matchA} title="Matchs Grupo A" />
      }
      {
        matchB && <GroupTable data={matchB}  title="Match Grupo B" />
      }
    </Modal>
  );
};
TournamentDetail.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  id: PropTypes.number,
};

export default TournamentDetail;
