import { Tag } from "antd";

const renderTagColumn = tag => {
  let color = "blue";

  switch (tag) {
    case "Finalizado":
      color = "green";

      break;

    case "En curso":
      color = "blue";

      break;

    case "Pendiente":
      color = "volcano";

      break;

    default:
      break;
  }

  return (
    <Tag color={color} key={tag}>
      {tag}
    </Tag>
  );
};

export default renderTagColumn;
