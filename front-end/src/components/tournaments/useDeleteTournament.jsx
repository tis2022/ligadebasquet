import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { removeTournament } from "../../store/dataSlice";
import { deleteTournament } from "../../services/tournament";

const useDeleteTournament = () => {
  const dispatch = useDispatch();

  return useCallback((id) => {
    deleteTournament(id)
    .then(() => {
      dispatch(removeTournament(id));
    });
  }, [dispatch]);
};

export default useDeleteTournament;
