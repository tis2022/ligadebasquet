import { Table } from "antd";
import PropTypes from "prop-types";

import { renderFlagColumn } from "./flagUtils";

import renderTagColumn from "./tagMatch";

const { Column, ColumnGroup } = Table;

const GroupTable = ({ data, title }) => (
  <Table
    dataSource={data}
    size="small"
    align="center"
    width="250px"
    pagination={false}
  >
    <ColumnGroup title={title}>
      <Column
        title="Dia - Hora"
        dataIndex="dateHour"
        key="dateHour"
        align="center"
        width="150px"
      />

      <Column
        title="Pais"
        dataIndex="country"
        key="country"
        align="center"
        render={renderFlagColumn}
        width="50px"
      />

      <Column
        title="Equipo"
        dataIndex="name"
        key="name"
        align="center"
        width="250px"
      />

      <Column title="VS" dataIndex="vs" key="vs" align="center" width="50px" />

      <Column
        title="Equipo"
        dataIndex="name2"
        key="name2"
        align="center"
        width="250px"
      />

      <Column
        title="Pais"
        dataIndex="country2"
        key="country2"
        align="center"
        render={renderFlagColumn}
        width="50px"
      />

      <Column
        title="Tags"
        dataIndex="tags"
        key="tags"
        align="center"
        render={renderTagColumn}
        width="60px"
      />
    </ColumnGroup>
  </Table>
);

GroupTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array
};

export default GroupTable;
