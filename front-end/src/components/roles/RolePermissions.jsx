import { useCallback, useEffect } from "react";
import { Form } from "react-final-form";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "antd";

import { updateBanner } from "../../store/bannerSlice";
import { config } from "../../services/api-service";
import useGetPermissions from "../hooks/useGetPermissions";
import RolePermissionsBody from "./RolePermissionsBody";

const RolePermissions = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { roleId } = useParams();
  const { fetchPermissionsByRole } = useGetPermissions(roleId);

  const rolePermissions = useSelector(state => state.data.rolePermissions);
  const initialValues = {
    permissions: rolePermissions.map(({ id }) => id)
  };

  useEffect(() => {
    fetchPermissionsByRole(roleId);
  }, [fetchPermissionsByRole, roleId]);

  const onSubmit = useCallback(
    formData => {
      fetch(`${config.baseURL}/api/permissions/${roleId}`, {
        method: "PUT",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          navigate(-1);
          dispatch(
            updateBanner({
              message: "Permisos Actualizados exitosamente",
              type: "success"
            })
          );
        })
        .catch(error => {
          dispatch(
            updateBanner({
              message: error.message,
              type: "error"
            })
          );
        });
    },
    [dispatch, navigate, roleId]
  );

  return (
    <Modal
      title="Asignar Permisos por Rol"
      visible
      footer={null}
      onCancel={() => navigate(-1)}
    >
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        render={({ handleSubmit, errors }) => (
          <RolePermissionsBody handleSubmit={handleSubmit} errors={errors} />
        )}
      />
    </Modal>
  );
};

export default RolePermissions;
