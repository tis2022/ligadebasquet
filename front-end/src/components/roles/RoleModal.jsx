import { useCallback } from "react";
import { Form } from "react-final-form";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "antd";
import PropTypes from "prop-types";
import isEmpty from "lodash/fp/isEmpty";

import RoleModalBody from "./RoleModalBody";
import useGetRoles from "./useGetRoles";
import { updateBanner } from "../../store/bannerSlice";
import { config } from "../../services/api-service";

const defaultValues = {
  role_id: 2
};

const RoleModal = ({ title }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { roleId } = useParams();
  const roleDataFromStore = useSelector(
    state =>
      state.data.roles.filter(role => role.id === parseInt(roleId, 10))[0]
  );
  const initialValues = roleDataFromStore ?? defaultValues;
  const isEdit = !isEmpty(roleDataFromStore);
  const { getRoles } = useGetRoles();

  const onSubmit = useCallback(
    formData => {
      const endpointTail = roleId ? `/${roleId}` : "";
      const message = roleId
        ? "Rol editado exitosamente."
        : "Rol añadido exitosamente.";

      fetch(`${config.baseURL}/api/roles${endpointTail}`, {
        method: roleId ? "PUT" : "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(() => {
          navigate(-1);
          dispatch(
            updateBanner({
              message,
              type: "success"
            })
          );
          getRoles();
        })
        .catch(error => {
          dispatch(
            updateBanner({
              message: error.message,
              type: "error"
            })
          );
        });
    },
    [dispatch, navigate, getRoles, roleId]
  );

  return (
    <Modal title={title} visible footer={null} onCancel={() => navigate(-1)}>
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        render={({ handleSubmit, errors }) => (
          <RoleModalBody
            handleSubmit={handleSubmit}
            errors={errors}
            isEdit={isEdit}
          />
        )}
      />
    </Modal>
  );
};

RoleModal.propTypes = {
  title: PropTypes.string.isRequired
};

export default RoleModal;
