import { Button, Select } from "antd";
import { useSelector } from "react-redux";
import { Field } from "react-final-form";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

import isEmpty from "lodash/fp/isEmpty";

import ErrorField from "../common/ErrorField";

const RolePermissionsBody = ({ handleSubmit, errors }) => {
  const navigate = useNavigate();
  const permissions = useSelector(state => state.data.permissions);
  const options = permissions.map(permission => ({
    label: permission.label,
    value: permission.id
  }));
  // const type = useSelector(state => state.user.type);

  return (
    <div>
      <Field name="permissions">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Permisos: *</div>
            <div className="form-field">
              <Select
                mode="multiple"
                allowClear
                style={{ width: "100%" }}
                placeholder="Seleccionar los permisos del rol"
                options={options}
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => navigate("/roles")}
          type="text"
          className="cancel-btn"
        >
          Cancelar
        </Button>
        <Button
          onClick={handleSubmit}
          type="primary"
          className="save-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

RolePermissionsBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

export default RolePermissionsBody;
