import { Field } from "react-final-form";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

import { Button, Input } from "antd";
import isEmpty from "lodash/fp/isEmpty";

import ErrorField from "../common/ErrorField";

const RoleModalBody = ({ handleSubmit, errors }) => {
  const navigate = useNavigate();
  // const type = useSelector(state => state.user.type);

  return (
    <div>
      <Field name="name">
        {({ input, meta }) => (
          <div className="field-container">
            <div>Nombre Rol: *</div>
            <div className="form-field">
              <Input
                required
                minLength="3"
                maxLength="20"
                status={meta.error && meta.touched ? "error" : undefined}
                placeholder="Nombre Rol"
                {...input}
              />
              {meta.error && meta.touched && <ErrorField error={meta.error} />}
            </div>
          </div>
        )}
      </Field>
      <div className="modal-footer">
        <Button
          onClick={() => navigate("/roles")}
          type="text"
          className="cancel-btn"
        >
          Cancelar
        </Button>
        <Button
          onClick={handleSubmit}
          type="primary"
          className="save-btn"
          disabled={!isEmpty(errors)}
        >
          Guardar
        </Button>
      </div>
    </div>
  );
};

RoleModalBody.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

export default RoleModalBody;
