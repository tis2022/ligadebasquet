import { useCallback, useMemo } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { Table, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import useGetRoles from "./useGetRoles";

import "antd/dist/antd.css";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  }
];

const RoleList = () => {
  const { roles } = useGetRoles();

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: (_, record) =>
        record.id !== 1 ? (
          <>
            <Button
              type="primary"
              success
              onClick={() => handleClick("edit", record)}
              key="edit"
              className="edit-btn"
            >
              Editar
            </Button>
            <Button
              type="primary"
              success
              onClick={() => handleClick("permisos", record)}
              key="edit"
              className="edit-btn"
            >
              Permisos
            </Button>
            <Button
              type="primary"
              danger
              onClick={() => handleClick("delete", record)}
              key="delete"
            >
              Eliminar
            </Button>
          </>
        ) : null
    }),
    [handleClick]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      <Button
        type="primary"
        onClick={() => navigate("add")}
        icon={<PlusOutlined />}
      >
        Agregar Rol
      </Button>
      <Table dataSource={roles} columns={columns} />
      <Outlet />
    </div>
  );
};

export default RoleList;
