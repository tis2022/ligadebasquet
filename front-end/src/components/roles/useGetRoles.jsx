import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { config } from "../../services/api-service";

import { roleList } from "../../store/dataSlice";

const useGetRoles = () => {
  const dispatch = useDispatch();
  const roles = useSelector(state => state.data.roles);

  const getRoles = useCallback(() => {
    fetch(`${config.baseURL}/api/roles`)
      .then(data => data.json())
      .then(data => {
        dispatch(roleList(data));
      });
  }, [dispatch]);

  useEffect(() => {
    getRoles();
  }, [getRoles]);

  return { roles, getRoles };
};

export default useGetRoles;
