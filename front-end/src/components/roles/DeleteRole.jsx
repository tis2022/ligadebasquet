import { Modal } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import { config } from "../../services/api-service";
import { updateBanner } from "../../store/bannerSlice";
import useGetRoles from "./useGetRoles";

const DeleteRole = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { roleId } = useParams();
  const { getRoles } = useGetRoles();

  const onSubmit = useCallback(() => {
    fetch(`${config.baseURL}/api/roles/${roleId}`, {
      method: "DELETE"
    }).then(() => {
      navigate(-1);
      getRoles();
      dispatch(
        updateBanner({
          message: "Rol eliminado exitosamente.",
          type: "success"
        })
      );
    });
  }, [dispatch, getRoles, navigate, roleId]);

  return (
    <Modal
      title="Eliminar Rol"
      visible
      onOk={onSubmit}
      onCancel={() => navigate(-1)}
    >
      Esta seguro que desea eliminar este rol?
    </Modal>
  );
};

export default DeleteRole;
