import { useCallback, useMemo, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { Table, Button } from "antd";
import useGetTeams from "../teams/useGetTeams";

const defaultColumns = [
  {
    title: "Nombre",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Categoría",
    dataIndex: "category_id",
    key: "category_id"
  },
  {
    title: "País",
    dataIndex: "country",
    key: "country"
  },
  {
    title: "Puntos",
    dataIndex: "points",
    key: "points"
  }
];

const TableTeams = () => {
  const { teams, getTeams } = useGetTeams();

  const navigate = useNavigate();
  const handleClick = useCallback(
    (buttonType, record) => {
      navigate(`${record.id}/${buttonType}`);
    },
    [navigate]
  );

  useEffect(() => {
    getTeams();
  }, [getTeams]);

  const actionsColumn = useMemo(
    () => ({
      title: "Acciones",
      dataIndex: "acciones",
      key: "acciones",
      render: (_, record) => (
        <>
          <Button
            type="primary"
            disabled
            danger
            onClick={() => handleClick("jugadores", record)}
            key="players"
          >
            Ver jugadores
          </Button>
          <Outlet />
        </>
      )
    }),
    [handleClick]
  );

  const columns = useMemo(
    () => [...defaultColumns, actionsColumn],
    [actionsColumn]
  );

  return (
    <div className="App">
      <Table dataSource={teams} columns={columns} />
      <Outlet />
    </div>
  );
};

export default TableTeams;
