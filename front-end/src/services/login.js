import ApiService from "./api-service";

const URL_LOGIN = "/api/login";
const URL_RESET_PASSWORD = "/api/resetPassword";

const login = async data => {
  const response = await ApiService.post(URL_LOGIN, data);
  return response.data;
};

export const resetPassword = async data => {
  const response = await ApiService.put(URL_RESET_PASSWORD, data);
  return response.data;
};

export default login;
