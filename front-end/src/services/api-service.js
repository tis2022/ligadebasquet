import axios from "axios";

export const config = {
  // eslint-disable-next-line no-undef
  baseURL: "http://elysium.tis.cs.umss.edu.bo"
};

const ApiService = (() => {
  const service = axios.create(config);
  return {
    get(url, params = {}) {
      return service.get(url, params);
    },
    post(url, data) {
      return service.post(url, data);
    },
    delete(url, data) {
      return service.delete(url, data);
    },
    put(url, data) {
      return service.put(url, data);
    }
  };
})();

export default ApiService;
