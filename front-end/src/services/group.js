import ApiService from "./api-service";

const URL_GROUPS = "/api/groups";

const getGroups = async () => {
  const response = await ApiService.get(URL_GROUPS);
  return response.data;
};

// eslint-disable-next-line import/prefer-default-export
export { getGroups };
