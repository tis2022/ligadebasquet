import ApiService from "./api-service";

const URL_TOURNAMENTS = "/api/tournaments";

const addTournament = async data => {
  const response = await ApiService.post(URL_TOURNAMENTS, data);
  return response.data;
};

const updateTournament = async data => {
  const response = await ApiService.put(`${URL_TOURNAMENTS}/${data.id}`, data);
  return response.data;
};

const deleteTournament = async id => {
  const response = await ApiService.delete(`${URL_TOURNAMENTS}/${id}`);
  return response.data;
};

export { addTournament, deleteTournament, updateTournament };
