import ApiService from "./api-service";

const URL_REGISTRATIONS = "/api/registration";

const getRegistrationsbyTournament = async id => {
  const response = await ApiService.get(`${URL_REGISTRATIONS}?tournament=${id}`);
  return response.data;
};

// eslint-disable-next-line import/prefer-default-export
export { getRegistrationsbyTournament };
