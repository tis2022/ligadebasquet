import { render } from "react-dom";
import { Provider } from "react-redux";

import AppRoutes from "./routes/routes";
import store from "./store/store";

import "./config/theme";

render(
  <Provider store={store}>
    <AppRoutes />
  </Provider>,
  document.getElementById("root")
);
